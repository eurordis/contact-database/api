develop:
	pip install -e .[test,dev]

test:
	py.test -vx

download-db:
	scp -r eurordis.prod:/srv/eurordis/backup.gz ./tmp/

restore-db:
	mongorestore --drop --gzip --archive=./tmp/backup.gz

restore-fixtures:
	wget -O ./tmp/fixtures.gz https://gitlab.com/eurordis/contact-database/api/-/raw/master/db-anonymizer/fixtures.gz?inline=false
	mongorestore --gzip --archive=./tmp/fixtures.gz --nsFrom eurordis-anonymized --nsTo eurordis
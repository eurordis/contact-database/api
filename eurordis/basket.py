import csv
import io
import re
from collections import defaultdict

from mongoengine.errors import ValidationError
from openpyxl import Workbook
from openpyxl.writer.excel import save_virtual_workbook

from . import constants, mailchimp, config
from .helpers import choose_language, format_for_tabular
from .models import Contact, Disease, Group, Keyword, Person, Role

FIELDS = [
    "pk",
    "first_name",
    "last_name",
    "email",
    "title",
    "phone",
    "cell_phone",
    "languages",
    "country_label",
    "country",
    "diseases",
    "keywords",
    "groups",
    "groupings",
    "kind",
    "role",
    "is_primary",
    "is_default",
    "is_voting",
    "organisation",
    "acronym",
    "eurordis_membership",
    "rdi_membership",
    "comment",
    "website",
    "address",
    "postcode",
    "city",
    "created_by",
    "created_at",
    "modified_by",
    "modified_at",
]


def load(resources):
    out = []
    pks = set()
    for resource in resources:
        if resource.get("person"):
            pks.add(resource["person"])
        if resource.get("organisation"):
            pks.add(resource["organisation"])
    roles = {
        (r["person"], r["organisation"]): r
        for r in Role._get_collection().find(
            {
                "$or": [
                    {"person": {"$in": list(pks)}},
                    {"organisation": {"$in": list(pks)}},
                ],
                "is_archived": {"$ne": True}
            }
        )
    }
    default_organisation = defaultdict(list)
    default_contacts = defaultdict(list)
    for role in roles.values():
        default_contacts[role["organisation"]].append(role["person"])
        default_organisation[role["person"]].append(role["organisation"])
    # Also load extra relations (so we can deal with default organisation later).
    pks = set(pks) | set([pk for pair in roles.keys() for pk in pair])
    contacts = {
        c["_id"]: c
        for c in Contact._get_collection().find(
            {"_id": {"$in": list(pks)}}, FIELDS + ["name", "membership"]
        )
    }
    pks = set()
    authorship = set()
    for contact in contacts.values():
        pks = pks.union(contact.get("diseases") or [])
        authorship.add(contact["modified_by"])
        authorship.add(contact["created_by"])
    authorship = {
        pk: email or f"{first_name} {last_name}"
        for pk, email, first_name, last_name in Person.objects(pk__in=authorship)
        .scalar("pk", "email", "first_name", "last_name")
        .no_dereference()
    }
    diseases = {
        d["_id"]: d["name"]
        for d in Disease._get_collection().find({"_id": {"$in": list(pks)}}, ["name"])
    }
    groups = {g.pk: g for g in Group.objects.filter()}
    keywords = {k.pk: k.name for k in Keyword.objects.filter()}
    for resource in resources:
        person, organisation = None, None
        if resource.get("person"):
            try:
                person = contacts[resource["person"]]
            except KeyError:
                continue
            pk = person["_id"]
            if "organisation" not in resource:
                contexts = []
                for ref in default_organisation[resource["person"]]:
                    contact = contacts[ref]
                    role = roles[resource["person"], ref]
                    if not role.get("is_archived"):
                        # default role's email
                        if role.get("is_default") and (role.get("email") or person.get("email")):
                            priority = 1
                        # primary role's email
                        elif role.get("is_primary") and (role.get("email") or person.get("email")):
                            priority = 2
                        # any role's email
                        elif role.get("email"):
                            priority = 3
                        # default role organisation's email
                        elif role.get("is_default") and contact.get("email"):
                            priority = 4
                        # primary role organisation's email
                        elif role.get("is_primary") and contact.get("email"):
                            priority = 5
                        # any role organisation's email
                        elif contact.get("email"):
                            priority = 6
                        # last choice if no email is found
                        else:
                            priority = 7
                        contexts.append((ref, priority))
                contexts.sort(key=lambda x: x[1])
                if contexts:
                    resource["organisation"] = contexts[0][0]
        if resource.get("organisation"):
            try:
                organisation = contacts[resource["organisation"]]
            except KeyError:
                continue
            if "person" not in resource:
                pk = organisation["_id"]
            if not organisation.get("email") and "person" not in resource:
                for ref in default_contacts[resource["organisation"]]:
                    contact = contacts[ref]
                    role = roles[ref, resource["organisation"]]
                    email = role.get("email") or contact.get("email")
                    if email:
                        person = contact
                        if role.get("is_primary"):
                            break
        # Different fields for RDI and EURORDIS membership
        if organisation and organisation.get("membership"):
            organisation["eurordis_membership"] = []
            organisation["rdi_membership"] = []
            for membership in organisation.get("membership"):
                if membership.get("organisation") == config.EURORDIS_PK:
                    organisation["eurordis_membership"].append(membership)
                elif membership.get("organisation") == config.RDI_PK:
                    organisation["rdi_membership"].append(membership)
            del organisation["membership"]
        if person and organisation:
            try:
                role = roles[(person["_id"], organisation["_id"])]
            except KeyError:
                continue
            data = load_from_role(role=role, person=person, organisation=organisation)
        elif person:
            data = load_from_person(person=person)
        elif organisation:
            data = load_from_organisation(organisation)
        else:
            raise ValueError(f"Invalid resource {resource}")
        data["pk"] = pk
        # Reset to original values.
        if organisation and pk == organisation["_id"]:
            for key in ["modified_by", "created_by", "modified_at", "created_at"]:
                data[key] = organisation.get(key)
            # Reset to original country if organisation has one
            if organisation.get("country"):
                data["country"] = organisation.get("country")
        data = format_for_tabular(
            raw=data,
            diseases=diseases,
            groups=groups,
            keywords=keywords,
            authorship=authorship,
        )
        if data.get("country"):
            data["country_label"] = constants.COUNTRIES.get(data["country"])
        out.append(
            {
                k: v
                for k, v in data.items()
                if v and k in FIELDS + ["person_pk", "organisation_pk"]
            }
        )
    return out


def load_from_person(person):
    data = person.copy()
    # Basket resource are a merge of Person + Role + Organisation, so we
    # need to use custom keys to prevent clashes.
    data["person_pk"] = data["_id"]
    return data


def load_from_organisation(organisation):
    data = organisation.copy()
    data["organisation_pk"] = data["_id"]
    data["organisation"] = data["name"]
    return data


def load_from_role(role, person, organisation):
    data = load_from_person(person)
    groups = role.get("groups") or []
    groups = [g for g in groups if g.get("is_active")]
    data.update(
        {
            "role": role.get("name"),
            "organisation": organisation["name"],
            "organisation_pk": organisation["_id"],
            "is_primary": bool(role.get("is_primary")),
            "is_default": bool(role.get("is_default")),
            "is_voting": bool(role.get("is_voting")),
            "groups": groups,
        }
    )
    for key, value in organisation.items():
        if key == "diseases":
            continue  # Do not override person value, we want to combine values.
        if data.get(key) in ("", None, []):
            data[key] = value
    data.setdefault("diseases", [])
    data["diseases"].extend(organisation.get("diseases") or [])
    if role.get("email"):
        data["email"] = role["email"]
    elif not data.get("email"):
        data["email"] = organisation.get("email")
    return data


def export_as_csv(resources, dialect="excel"):
    output = io.StringIO()
    writer = csv.DictWriter(
        output, fieldnames=FIELDS, extrasaction="ignore", dialect=dialect
    )
    writer.writeheader()
    writer.writerows(resources)
    output.seek(0)
    return output.read()


def export_as_xls(resources):
    wb = Workbook(write_only=True)
    ws = wb.create_sheet()
    ws.title = "Eurordis Contact Database"  # Less than 32 chars.
    ws.append(FIELDS)
    for resource in resources:
        ws.append(resource.get(field) for field in FIELDS)
    return save_virtual_workbook(wb)


async def export_as_mailchimp_segments(resources, name, languages):
    if not name or not languages:
        raise ValueError("name and languages must not be empty")
    batches = {l: [] for l in languages}
    for resource in resources:
        email = resource.get("email")
        if not email:
            continue
        language = choose_language(
            (resource.get("languages") or "").split(","), languages
        )
        batches[language].append(email)
    batches = {k: v for k, v in batches.items() if v}
    for language, emails in batches.items():
        await mailchimp.create_tag(f"{name}_{language}", emails)
    return {"report": f"Created {len(batches)} tags"}


def mass_edit(resources, field, operator, value):
    reports = []
    for raw in resources:
        report = {"raw": raw, "status": "error"}
        reports.append(report)
        id_ = raw.get("person") or raw.get("organisation")
        if not id_:
            report["error"] = "Invalid input data"
            continue
        if field == "groups":
            person_id = raw.get("person")
            organisation_id = raw.get("organisation")
            if not person_id or not organisation_id:
                report["error"] = "Can only apply groups to roles"
                continue
            try:
                resource = Role.get(person=person_id, organisation=organisation_id)
            except Role.DoesNotExist:
                report["error"] = "Role does not exist"
                continue
        else:
            try:
                resource = Contact.get(id_)
            except Contact.DoesNotExist:
                report["error"] = "Resource does not exist"
                continue
        mass_edit_resource(resource, field, operator, value, report)
    return reports


def mass_edit_resource(resource, field, operator, value, report):
    if field not in resource._fields:
        report["error"] = f"Invalid field {field}"
        return
    if operator == "push":
        old_value = getattr(resource, field)
        try:
            old_value.append(value)
        except AttributeError:
            report["error"] = f"Invalid operator {operator}"
            return
        try:
            resource.simple_update({field: old_value})
        except (ValidationError, ValueError):
            report["error"] = f"Invalid value: {value}"
        else:
            report["status"] = "success"
    elif operator == "pull":
        old_value = getattr(resource, field)
        if field in ["keywords", "diseases"]:
            old_value = [v for v in old_value if v.pk != value]
        elif field == "groups":
            old_value = [v for v in old_value if v.group.pk != value["group"]]
        else:
            try:
                old_value.remove(value)
            except AttributeError:
                report["error"] = f"Invalid operator {operator}"
                return
            except ValueError:
                report["warning"] = f"Value was not present: {value}"
                return
        resource.simple_update({field: old_value})
        report["status"] = "success"
    elif operator == "set":
        try:
            resource.simple_update({field: value})
        except (ValidationError, ValueError):
            report["error"] = f"Invalid value: {value}"
        else:
            report["status"] = "success"
    else:
        report["error"] = f"Unsupported operator {operator}"


def from_refs(refs, resource=Person):
    refs = [r.lower() for r in refs]  # Allow to match found/notfound later.
    re_refs = [re.compile(f"^{ref}$", re.IGNORECASE) for ref in refs]
    resource_name = resource.resource_name()
    other = "person" if resource_name == "organisation" else "organisation"
    roles = list(
        Role._get_collection().find(
            {"email": {"$in": re_refs}},
            {"person": True, "organisation": True, "_id": False, "email": True},
        )
    )
    refs.extend([r[resource_name] for r in roles])
    resources = list(
        Contact._get_collection().find(
            {
                "$or": [
                    {"_id": {"$in": refs}, "_cls": resource._class_name},
                    {"email": {"$in": re_refs}, "_cls": resource._class_name},
                ]
            },
            {"pk": True, "email": True},
        )
    )
    pks = [r["_id"] for r in resources]
    defaults = {
        r[resource_name]: r[other]
        for r in Role._get_collection().find(
            {resource_name: {"$in": pks}, "is_default": True},
            {"person": True, "organisation": True},
        )
    }
    found = set([ref.lower() for r in resources for ref in r.values()])
    found |= set([r["email"] for r in roles if r["email"]])
    not_found = set(refs) - found
    roles = {r[resource_name]: r[other] for r in roles}
    resources = []
    for pk in pks:
        resource = {resource_name: pk}
        if pk in roles:
            resource[other] = roles[pk]
        elif pk in defaults:
            resource[other] = defaults[pk]
        resources.append(resource)
    return {"resources": resources, "not_found": not_found}

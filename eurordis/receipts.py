from datetime import date
from io import BytesIO
from pathlib import Path

from fpdf import fpdf

from . import config


BODY = """Dear {person},

We have received your {amount} € in payment of {organisation} membership fee for the year {year}.

We thank you for your continued support and look forward to further fruitful collaboration.

Best regards,"""


class PDF(fpdf.FPDF):
    def __init__(self, role, year, *args, **kwargs):
        kwargs["font_cache_dir"] = "/tmp/"
        super().__init__(*args, **kwargs)
        EURORDIS = role.organisation.get_organisation(config.EURORDIS_STR)
        membership = role.organisation.organisation_membership(EURORDIS)
        if not membership:
            raise ValueError("This organisation is not a member")
        fee = membership.fee_for(year)
        if not fee:
            raise ValueError(f"No fee for year {year}")
        self.fee = fee
        self.role = role
        self.year = year
        self.root = Path(__file__).parent
        self.add_font("Corbel", "", self.root / "static/font/Corbel.ttf", uni=True)
        self.add_font(
            "Corbel", "B", self.root / "static/font/Corbel Bold.ttf", uni=True
        )
        self.set_title("Eurordis Membership Receipt")

    def __call__(self, path=None):
        self.fee.receipt_sent = True
        self.set_auto_page_break(False)
        self.set_margins(30, 20)
        self.add_page()
        self.letterhead()
        self.headline()
        self.body()
        self.signature()
        self.footer()
        if path:
            return self.output(path)
        f = BytesIO(self.output())
        f.filename = f"receipt_{self.year}.pdf"
        f.content_type = "application/pdf"
        return f

    def sent_date(self):
        # Allow to monkeypatch date during tests.
        return date.today()

    def header(self):
        self.image(self.root / "static/img/logo.png", w=50)
        self.ln(20)

    def letterhead(self):
        self.set_font("Corbel", "B", 11)
        # Move to the right
        self.cell(80)
        org = self.role.organisation
        self.multi_cell(w=60, h=5, txt=str(org), ln=2, align="L")
        self.set_font("Corbel", "", 11)
        self.cell(w=0, h=5, txt=str(self.role.person), ln=2, align="L")
        self.cell(w=0, h=5, txt=org.address or "", ln=2, align="L")
        self.cell(w=0, h=5, txt=f"{org.postcode} {org.city}", ln=2, align="L")
        self.cell(w=0, h=5, txt=org.country_label or "", ln=2, align="L")
        self.ln(5)
        self.cell(80)
        self.cell(
            w=0,
            h=5,
            txt=f"Paris, {self.sent_date().strftime('%d %B %Y')}",
            ln=2,
            align="L",
        )
        self.ln(20)

    def headline(self):
        self.set_text_color(r=50, g=150, b=250)
        self.set_font("Corbel", "B", 16)
        txt = f"Receipt Membership Fees {self.year}"
        self.cell(w=0, h=5, txt=txt, ln=2, align="C")
        self.ln(20)

    def body(self):
        self.set_text_color(r=0)
        self.set_font("Corbel", "", 11)
        txt = BODY.format(
            person=self.role.person,
            organisation=self.role.organisation,
            amount=int(self.fee.amount),
            year=self.year,
        )
        self.multi_cell(w=0, h=5, txt=txt, ln=2, align="L")
        self.ln(5)

    def signature(self):
        self.image(self.root / "static/img/signature.png", w=33)
        self.cell(w=0, h=5, txt="Anja Helm", ln=2, align="L")
        self.cell(
            w=0,
            h=5,
            txt="Senior Manager of Relations With Patient Organisations",
            ln=2,
            align="L",
        )

    def footer(self):
        self.set_y(-30)
        self.set_text_color(r=50, g=150, b=250)
        self.set_font("Corbel", "B", 12)
        self.cell(w=0, h=5, txt="EURORDIS-Rare Diseases Europe", ln=2, align="L")
        self.set_font("Corbel", "", 11)
        self.cell(
            w=0,
            h=5,
            txt="Plateforme Maladies Rares • 96 rue Didot 75014 Paris • France",
            ln=2,
            align="L",
        )
        self.cell(
            w=0,
            h=5,
            txt="Tél. +33 1 56 53 52 10 • Fax +33 1 56 53 52 15 • eurordis@eurordis.org",
            ln=2,
            align="L",
        )


def fee(role, year):
    return PDF(role, year)

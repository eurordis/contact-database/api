import random
import string
import sys

from . import (
    config,
    helpers,
    constants
)
from .loggers import logger
from .models import Event, Keyword, Organisation, Person, Role, Membership, RoleToEvent
from .mailchimp import get, get_sync_lists

def monitor_mailchimp_email():
    email_random_data = ''.join(random.choices(string.ascii_letters, k=5))
    return f"mailchimp.test.{email_random_data}@somewhere.org"

def monitor_mailchimp_add_data():
    try:
        mailchimp_monitoring_init_keyword = Keyword.get(config.MAILCHIMP_MONITORING_INIT_KEYWORD)
    except Keyword.DoesNotExist:
        sys.exit("MAILCHIMP_MONITORING_INIT_KEYWORD invalid or not defined. Aborting.")
    # Person testing data
    person = Person(
        title="mr",
        first_name="Mail",
        last_name="Chimp",
        email=monitor_mailchimp_email(),
        created_at=helpers.utcnow(),
        modified_at=helpers.utcnow(),
        keywords=[mailchimp_monitoring_init_keyword]
    ).save()
    try:
        test_keyword = Keyword.get(config.MAILCHIMP_TEST_KEYWORD)
    except Keyword.DoesNotExist:    
        person.delete()
        sys.exit("MAILCHIMP_TEST_KEYWORD not defined. Aborting")
    person.keywords.append(test_keyword)
    person.save()
    # Organisation testing data
    EURORDIS = Organisation.objects.get(filemaker_pk=config.EURORDIS_FM_PK)
    RDI = Organisation.objects.get(pk=config.RDI_PK)
    organisation = Organisation(
        name=constants.MAILCHIMP_MONITORING_MEMBER_ORGANISATION,
        membership=[Membership(organisation=EURORDIS, status="f"), Membership(organisation=RDI, status="f")],
        kind=60,
    ).save()
    # Event testing data (for alumni)
    event = Event(
        name=constants.MAILCHIMP_MONITORING_EVENT,
        kind="digital_school",
        start_date="2019/03/02",
        duration=1,
        location="Brussels",
    ).save()
    # Role testing data
    role = Role(person=person.pk, organisation=organisation, events=[RoleToEvent(event=event, role="Participant")]).save()
    return event, role, organisation, person

def monitor_mailchimp_delete_data(*args):
    for element in args:
        if element:
            try:
                element.delete()
            except KeyError:
                continue

async def monitor_mailchimp_verify(emails_to_verify, context, deleted=False):
    if not config.MAILCHIMP_APIKEY:
        print(constants.MAILCHIMP_MISSING_KEY_ERROR)
        return []
    failed = []
    for name, props in get_sync_lists().items():
        list_id = props["id"]
        if not list_id:
            print(f"No key for list {name}. Skipping.")
            continue
        resp = await get(f"lists/{list_id}/members?fields=total_items")
        total = resp.json()["total_items"]
        emails = []
        count = 100
        for offset in range(0, total, count):
            resp = await get(
                f"lists/{list_id}/members?fields=members.email_address,members.status,members.id&offset={offset}&count={count}"
            )
            emails.extend([member["email_address"].lower() for member in resp.json()["members"]])
        # Verify
        for email in emails_to_verify:
            if not deleted and email.lower() not in emails:
                message = f"Mailchimp test failed looking for email {email} in {name} (Context: {context})"
                failed.append(message)
                logger.debug(message)
            if deleted and email.lower() in emails:
                message = f"Mailchimp test failed looking for deleted email {email} in {name} (Context: {context})"
                failed.append(message)
                logger.debug(message)
    return failed


async def monitor_init():
    failed = []
    # Verify MAILCHIMP_MONITORING_INIT_KEYWORD persons
    try:
        mailchimp_monitoring_init_keyword = Keyword.get(
            config.MAILCHIMP_MONITORING_INIT_KEYWORD)
    except Keyword.DoesNotExist:
        print("MAILCHIMP_MONITORING_INIT_KEYWORD invalid or not defined. Aborting.")
        return []
    monitoring_init_persons = Person.objects.filter(
        keywords=mailchimp_monitoring_init_keyword)
    monitoring_init_persons_emails = [
        person.email for person in monitoring_init_persons]
    if monitoring_init_persons_emails:
        failed_added_verification = await monitor_mailchimp_verify(monitoring_init_persons_emails, "add a user")
        failed.extend(failed_added_verification)
    return failed, monitoring_init_persons


async def after_batch():
    failed = []
    failed_init, monitoring_init_persons = await monitor_init()
    failed.extend(failed_init)
    # Delete testing data
    # Using specific conditions to avoid deleting other data by accident
    for person in monitoring_init_persons:
        role = None
        if person.roles:
            for person_role in person.roles:
                if person_role.organisation and person_role.organisation.name == constants.MAILCHIMP_MONITORING_MEMBER_ORGANISATION:
                    role = person_role
        organisation = role.organisation if role else None
        event = None
        if role and role.events:
            for role_to_event in role.events:
                if role_to_event.event and role_to_event.event.name == constants.MAILCHIMP_MONITORING_EVENT:
                    event = role_to_event.event
        monitor_mailchimp_delete_data(event, role, organisation, person)
    # TODO: uncomment this part after verifying Mailchimp monitoring logs, make sure the tests work
    # Send alerts
    # if failed:
    #     messages = "\n".join(failed)
    #     body = emails.MAILCHIMP_REPORT.format(messages=messages)
    #     emails.send(to=config.MAILCHIMP_REPORT_EMAILS, subject="Contact Database Mailchimp Report", body=body)

import re
from collections import defaultdict
from datetime import timedelta
from traceback import print_exc
from urllib.parse import urlparse

import jwt
from mongoengine import connect
from mongoengine.errors import (
    DoesNotExist,
    FieldDoesNotExist,
    NotUniqueError,
    ValidationError,
)
from roll import HttpError
from roll import Query as BaseQuery
from roll import Request as BaseRequest
from roll import Response as BaseResponse
from roll import Roll as BaseRoll
from roll.extensions import cors, options

from . import (
    activity,
    basket,
    config,
    constants,
    emails,
    helpers,
    importer,
    mailchimp,
    receipts,
    reports,
    session,
)
from .helpers import complete_country as complete_country_
from .helpers import complete_grouping as complete_grouping_
from .loggers import logger
from .mailchimp_monitoring import after_batch
from .models import (
    Contact,
    Disease,
    Event,
    Group,
    Keyword,
    Organisation,
    Person,
    RoleToEvent,
    Role,
    Version,
)
from .search.front import SearchQuery
from .search import back

try:
    import pkg_resources
except ImportError:  # pragma: no cover
    pass
else:
    if __package__:
        VERSION = pkg_resources.get_distribution(__package__).version


class Query(BaseQuery):

    resources = {"person": Person, "organisation": Organisation, "contact": Contact}

    def get_resource(self):
        resource = self.get("resource", None) or "contact"
        try:
            return self.resources[resource]
        except KeyError:
            raise HttpError(400, f"Unknown resource `{resource}`")


class Response(BaseResponse):
    def xlsx(self, body, filename):
        self.body = body
        mimetype = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
        self.headers["Content-Disposition"] = f'attachment; filename="{filename}.xlsx"'
        self.headers["Content-Type"] = f"{mimetype}; charset=utf-8"

    def csv(self, body, filename):
        self.body = body
        self.headers["Content-Disposition"] = f'attachment; filename="{filename}.csv"'
        self.headers["Content-Type"] = "text/csv; charset=utf-8"


class Request(BaseRequest):
    @property
    def hostname(self):
        return self.origin or f"https{self.host}"


class Roll(BaseRoll):
    Query = Query
    Response = Response
    Request = Request


app = Roll()
cors(app, methods="*", headers=["*", "api-key"])
options(app)


def configure():
    config.init()
    connect(
        name=config.DBNAME,
        host=config.DBHOST,
        port=config.DBPORT,
        alias="default",
        tz_aware=True,
    )
    back.init()


def auth_required(*args, **kwargs):
    if not args:  # Called with scope.
        return lambda *args: auth_required(*args, **kwargs)
    view = args[0]
    scope = kwargs.get("scope")

    def wrapper(*args, **kwargs):
        request = args[0]  # function view.
        if not hasattr(request, "headers"):  # method view.
            request = args[1]
        token = request.headers.get("API-KEY")
        if not token:
            logger.debug("No token provided on request for %s", request.url)
            raise HttpError(401, "No authentication token was provided.")
        try:
            decoded = jwt.decode(
                token, config.JWT_SECRET, algorithms=[config.JWT_ALGORITHM]
            )
        except (jwt.DecodeError, jwt.ExpiredSignatureError):
            logger.debug("Invalid token for %s: %s", request.url, token)
            raise HttpError(401, "Invalid token")
        user_id = decoded.get("sub")
        if not user_id:
            raise HttpError(401, "Unrecognized token.")
        try:
            person = Person.get(id=user_id)
        except Person.DoesNotExist:
            raise HttpError(401, "No user matches the provided token.")
        email = decoded["email"]
        scopes = person.compute_scopes(email)
        print(scope, scopes)
        if scope and scope not in scopes:
            logger.debug("Unauthorized call on %s by %s", request.url, person)
            raise HttpError(401, "Unauthorized.")
        logger.debug("Logged in %s with email %s and scopes %s", person, email, scopes)
        person._scopes = scopes
        request["user"] = person
        session.user_id.set(user_id)
        session.ghost.set(request.headers.get("GHOST") == "1")
        return view(*args, **kwargs)

    return wrapper


def jwt_token(person, email):
    exp = helpers.utcnow() + timedelta(days=7)
    return (
        jwt.encode(
            {"sub": str(person.id), "exp": exp, "email": email},
            config.JWT_SECRET,
            config.JWT_ALGORITHM,
        ),
        exp,
    )


@app.listen("headers")
async def on_headers(request, response):
    if not config.READONLY:
        return
    if request.path == "/token":
        return
    if request.path.startswith("/contact/export."):
        return
    if request.path == "/basket" and request.method in ["POST", "PUT"]:
        return
    if request.method not in ["GET", "OPTIONS"]:
        raise HttpError(405, "Contact Database is readonly")


@app.listen("startup")
async def on_startup():
    configure()


@app.listen("error")
async def json_error_response(request, response, error):
    if error.status == 500:  # This error as not yet been caught
        if isinstance(error.__context__, DoesNotExist):
            response.status = 404
            error.message = f"Resource not found: {error.__context__}"
        elif isinstance(
            error.__context__, (ValueError, NotUniqueError, FieldDoesNotExist)
        ):
            response.status = 422
            error.message = {"error": str(error.__context__)}
        elif isinstance(error.__context__, ValidationError):
            # Error.errors is a dict with ValidationError as values…
            error.message = helpers.format_validation_error(error.__context__)
            response.status = 422
        elif isinstance(error.__context__, AssertionError):
            response.status = 400
        else:
            print_exc()
    if isinstance(error.message, (str, bytes)):
        error.message = {"error": error.message}
    response.json = error.message


@app.route("/token", methods=["POST"])
async def request_token(request, response):
    data = request.json
    email = data.get("email")
    if not email or not email.endswith(config.ACCESS_EMAIL_DOMAINS):
        logger.debug("Unauthorized token request from %s", email)
        raise HttpError(403, {"error": "Cannot use this email"})
    person = Person.from_email(email)
    if not person:
        logger.debug("Token request from unknown email: %s", email)
        raise HttpError(403, {"error": "Cannot use this email"})
    eurordis_role = next(
        (
            r
            for r in person.roles
            if r.email and r.email.endswith(config.ACCESS_EMAIL_DOMAINS)
        ),
        None,
    )
    if eurordis_role and eurordis_role.is_archived:
        logger.debug("Unauthorized token request from %s", email)
        raise HttpError(403, {"error": "Cannot use this email"})
    response.status = 204
    token, exp = jwt_token(person, email)
    logger.debug("Token sent to %s", email)
    # In local, request.host is the API domain, not the frontend app one.
    link = f"{request.hostname}/access/{token}"
    if "localhost" in link or "127.0.0.1" in link:
        print(link)
    body = emails.ACCESS_GRANTED.format(
        person=person,
        link=link,
        date=exp.strftime("%Y/%m/%d"),
        time=exp.strftime("%H:%M"),
    )
    try:
        emails.send(to=email, subject="Contact Database Access", body=body)
    except SystemError:
        raise HttpError(500, "Unable to send email. Please contact the Eurordis Team.")


@app.route("/me")
@auth_required
async def profile(request, response):
    response.json = request["user"].as_resource()


@app.route("/history")
@auth_required
async def get_history(request, response):
    limit = request.query.int("limit", 10)
    action = request.query.get("action", None)
    field = request.query.get("field", None)
    response.json = {
        "data": [
            v.as_resource()
            for v in Version.history(limit=limit, action=action, field=field)
        ]
    }


@app.route("/history/export")
@auth_required
async def history_export(request, response):
    response.csv(reports.history(), "stasi")


@app.route("/search")
@app.route("/contact/search")
@auth_required
async def search(request, response):
    resource = request.query.get_resource()
    qs = resource.objects
    query = SearchQuery(request)
    if not query:
        raise HttpError(400, "Empty search query")
    qs = qs.no_dereference()
    # Allow to deal with multiple filters for a same key.
    filters = {"$and": query.filters} if query.filters else {}
    total = 0
    facets = request.query.bool("facets", True)
    pks = []
    qs = list(qs.context_search(query.q, limit=None, **filters))
    total = len(qs)
    if total:
        # Retrieve pks without limit so we can compute facet on whole dataset.
        pks = [r["pk"] for r in qs]
        if query.limit:
            qs = qs[: query.limit]
        insts = {
            r.pk: r.as_relation("diseases")
            for r in Contact.objects(pk__in=[r["pk"] for r in qs])
        }
        resources = qs
        for resource in resources:
            resource.update(insts[resource["pk"]])
        raw = (
            Role.objects.filter(
                __raw__={
                    "$or": [{"person": {"$in": pks}}, {"organisation": {"$in": pks}}]
                }
            )
            .scalar("person", "organisation", "is_primary", "is_default")
            .no_dereference()
        )
        roles = defaultdict(list)
        for person, organisation, is_primary, is_default in raw:
            role = {
                "person": person.id,
                "organisation": organisation.id,
                "is_primary": is_primary,
                "is_default": is_default,
            }
            roles[person.id].append(role)
            roles[organisation.id].append(role)
        for resource in resources:
            resource["roles"] = roles.get(resource["pk"], [])
            # Re-order roles
            for role in resource["roles"]:
                if role.get("is_default") and resource.get("role") and not resource.get("role").get("is_default"):
                    complete_role_information = Role.get(person=role.get(
                        "person"), organisation=role.get("organisation"))
                    complete_role_information.resource_fields = ["organisation", "email", "is_primary", "phone", "name"]
                    resource["role"] = complete_role_information.as_resource()
    else:
        resources = []
        total = 0
    response.json = {
        "data": resources,
        "total": total,
        "facets": query.compute_facets(pks) if facets else [],
    }


@app.route("/contact/complete")
async def complete_contact(request, response):
    resource = request.query.get_resource()
    q = request.query.get("q")
    limit = request.query.int("limit", 10)
    qs = resource.objects.search(q, limit)
    response.json = {"data": [d.as_relation() for d in qs]}


@app.route("/person/import", methods=["POST"])
@auth_required(scope="import")
async def person_import(request, response):
    report, _, _ = importer.process(request.files.get("data"))
    response.json = report


class ResourceHistory:
    model = None

    @auth_required
    async def on_get(self, request, response, id):
        response.json = {"data": [d.as_resource() for d in self.model.get(id).history]}


class ResourceList:
    model = None

    @auth_required
    async def on_post(self, request, response):
        response.json = self.model.from_document(request.json).save().as_resource()

    @auth_required
    async def on_get(self, request, response):
        limit = request.query.int("limit", 0)
        qs = self.model.objects.order_by("last_name", "name")
        if limit:
            qs = qs.limit(limit)
        # Case insensitive ordering.
        # cf https://github.com/MongoEngine/mongoengine/pull/2144
        qs._cursor.collation({"locale": "en"})
        response.json = {"data": [i.as_relation() for i in qs]}


class Resource:
    model = None

    @auth_required
    async def on_get(self, request, response, id):
        response.json = self.model.get(id).as_resource()

    @auth_required
    async def on_put(self, request, response, id):
        inst = self.model.get(id)
        inst.replace(request.json)
        response.json = inst.as_resource()

    @auth_required
    async def on_patch(self, request, response, id):
        inst = self.model.get(id)
        inst.simple_update(request.json)
        response.json = inst.as_resource()

    @auth_required(scope="delete")
    async def on_delete(self, request, response, id):
        inst = self.model.get(id)
        inst.delete()
        response.status = 204


@app.route("/person/{id}")
class PersonResource(Resource):
    model = Person


@app.route("/person")
class PersonResourceList(ResourceList):
    model = Person


@app.route("/person/{id}/history")
class PersonResourceHistory(ResourceHistory):
    model = Person


@app.route("/organisation/{id}")
class OrganisationResource(Resource):
    model = Organisation
    
    @auth_required
    async def on_get(self, request, response, id):
        inst = self.model.get(id)
        resource = inst.as_resource()
        # Remove archived roles
        resource["roles"] = [role.as_resource() for role in inst.unarchived_roles]
        response.json = resource

    @auth_required
    async def on_put(self, request, response, id):
        inst = self.model.get(id)
        inst.replace(request.json)
        resource = inst.as_resource()
        # Remove archived roles
        resource["roles"] = [role.as_resource() for role in inst.unarchived_roles]
        response.json = resource

    @auth_required
    async def on_patch(self, request, response, id):
        inst = self.model.get(id)
        inst.simple_update(request.json)
        resource = inst.as_resource()
        # Remove archived roles
        resource["roles"] = [role.as_resource() for role in inst.unarchived_roles]
        response.json = resource


@app.route("/organisation")
class OrganisationResourceList(ResourceList):
    model = Organisation


@app.route("/organisation/{id}/history")
class OrganisationResourceHistory(ResourceHistory):
    model = Organisation


@app.route("/event/{id:digit}")
class EventResource(Resource):
    model = Event


@app.route("/event")
class EventResourceList(ResourceList):
    model = Event


@app.route("/event/{id:digit}/history")
class EventResourceHistory(ResourceHistory):
    model = Event


@app.route("/group/{id:digit}")
class GroupResource(Resource):
    model = Group


@app.route("/group")
class GroupResourceList(ResourceList):
    model = Group


@app.route("/group/{id:digit}/history")
class GroupResourceHistory(ResourceHistory):
    model = Group


@app.route("/keyword/{id:digit}")
class KeywordResource(Resource):
    model = Keyword


@app.route("/keyword")
class KeywordResourceList(ResourceList):
    model = Keyword


@app.route("/keyword/{id:digit}/history")
class KeywordResourceHistory(ResourceHistory):
    model = Keyword


@app.route("/contact/{id:digit}/attachments", methods=["POST"])
@auth_required
async def post_attachment(request, response, id):
    inst = Contact.get(id)
    inst.add_attachment(request.files.get("data"))
    inst.save()


@app.route("/contact/{id:digit}/attachments/{file_id}", methods=["GET"])
@auth_required
async def get_attachment(request, response, id, file_id):
    inst = Contact.get(id)
    try:
        file = inst.attachments[file_id].get()
    except KeyError:
        raise HttpError(404, f"No attachment found with id {file_id}")
    response.headers["Access-Control-Expose-Headers"] = "X-Filename"
    response.headers["X-Filename"] = file.filename
    response.headers["Content-Type"] = f"{file.contentType}; charset=utf-8"
    response.body = file.read()


@app.route("/contact/{id:digit}/attachments/{file_id}", methods=["DELETE"])
@auth_required
async def delete_attachment(request, response, id, file_id):
    inst = Contact.get(id)
    try:
        file = inst.attachments[file_id]
    except KeyError:
        raise HttpError(404, f"No attachment found with id {file_id}")
    file.delete()
    del inst.attachments[file_id]
    inst.save()
    response.status = 204


@app.route("/event/complete")
async def complete_event(request, response):
    q = request.query.get("q")
    limit = request.query.int("limit", 10)
    qs = Event.objects.search(q, limit)
    response.json = {"data": [e.as_relation() for e in qs]}


@app.route("/group/complete")
async def complete_group(request, response):
    q = request.query.get("q")
    limit = request.query.int("limit", 10)
    qs = Group.objects.search(q, limit)
    response.json = {"data": [e.as_relation() for e in qs]}


@app.route("/keyword/complete")
async def complete_keyword(request, response):
    q = request.query.get("q")
    limit = request.query.int("limit", 10)
    for_person = request.query.bool("for_person", None)
    for_organisation = request.query.bool("for_organisation", None)
    filters = {}
    if for_person is not None:
        filters = {"for_person": for_person}
    if for_organisation is not None:
        filters = {"for_organisation": for_organisation}
    qs = Keyword.objects.search(q, limit, **filters)
    response.json = {"data": [e.as_relation() for e in qs]}


@app.route("/event/{id:digit}/import", methods=["POST"])
@auth_required(scope="import")
async def event_import(request, response, id):
    event = Event.get(id)
    reports, _, roles = importer.process(request.files.get("data"))
    for report, role in zip(reports, roles):
        if report["status"] == "error":
            continue
        if not role:
            report["status"] = "error"
            report["report"][
                "error"
            ] = "No organisation matched. Person created but not linked to event."
            continue
        attended = role.attended(event)
        event_role = report["raw"].get("role")
        if not attended or (event_role and attended.role != event_role):
            role.events.append(RoleToEvent(event=event, role=event_role))
            try:
                role.save()
            except ValidationError as err:
                errors = helpers.format_validation_error(err)
                report.update(errors)
                report["status"] = "error"
    response.json = reports


@app.route("/event/{id:digit}/export", methods=["GET"])
@auth_required
async def event_export(request, response, id):
    event = Event.get(id)
    response.xlsx(reports.attendees(event), f"event-{event.pk}-attendees")


@app.route("/disease/complete")
async def complete_diseases(request, response):
    q = request.query.get("q")
    limit = request.query.int("limit", 10)
    qs = Disease.search(q, limit)
    response.json = {"data": [d.as_relation() for d in qs]}


@app.route("/disease/{id:digit}")
@auth_required
async def get_disease(request, response, id):
    response.json = Disease.get(id).as_resource()


@app.route("/disease/{id:digit}", methods=["PATCH"])
@auth_required
async def patch_disease(request, response, id):
    inst = Disease.get(id)
    inst.ensure_editable_fields(request.json)
    inst.simple_update(request.json)
    response.json = inst.as_resource()


@app.route("/disease/{id:digit}/history")
class DiseaseResourceHistory(ResourceHistory):
    model = Disease


@app.route("/contact/export.{ext}", methods=["POST"])
@auth_required
async def export_contacts(request, response, ext):
    resources = basket.load(request.json["resources"])
    if ext == "tsv":
        response.body = basket.export_as_csv(resources, dialect="excel-tab")
        filename = "export.tab"
        mimetype = "text/tab-separated-values"
    elif ext == "xlsx":
        response.body = basket.export_as_xls(resources)
        filename = "export.xlsx"
        mimetype = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    elif ext == "csv":
        response.body = basket.export_as_csv(resources)
        filename = "export.csv"
        mimetype = "text/csv"
    else:
        raise HttpError(400, f"Unsupported format: {ext}")
    response.headers["Content-Disposition"] = f'attachment; filename="{filename}"'
    response.headers["Content-Type"] = f"{mimetype}; charset=utf-8"


@app.route("/basket", methods=["PATCH"])
@auth_required
async def mass_edit_contacts(request, response):
    data = request.json
    try:
        resources = data["resources"]
        field = data["field"]
        operator = data["operator"]
        value = data["value"]
    except KeyError:
        raise HttpError(400, "Invalid input")
    report = basket.mass_edit(resources, field, operator, value)
    response.json = report


@app.route("/report/fees")
@app.route("/report/fees.xlsx")
@auth_required
async def report_fees(request, response):
    year = request.query.int("year")
    response.xlsx(reports.fees(year), f"fees-report-{year}")


@app.route("/report/members.json")
async def report_members(request, response):
    response.json = {"data": reports.members()}


@app.route("/report/epag-members.json")
async def report_epag_members(request, response):
    response.json = reports.epag_members()


@app.route("/report/eurordis-members.xlsx")
@auth_required
async def report_eurordis_members_xlsx(request, response):
    response.xlsx(reports.members_xlsx(config.EURORDIS_STR), "members")


@app.route("/report/rdi-members.xlsx")
@auth_required
async def report_rdi_members_xlsx(request, response):
    response.xlsx(reports.members_xlsx(config.RDI_STR), "members")


@app.route("/report/eurordis-members-status.xlsx")
@auth_required
async def report_eurordis_members_status(request, response):
    response.xlsx(reports.members_status(config.EURORDIS_STR), "members-status")


@app.route("/report/rdi-members-status.xlsx")
@auth_required
async def report_rdi_members_status(request, response):
    response.xlsx(reports.members_status(config.RDI_STR), "members-status")


@app.route("/report/withdrawn-eurordis-members.xlsx")
@auth_required
async def report_withdrawn_eurordis_members(request, response):
    response.xlsx(reports.withdrawn_members(config.EURORDIS_STR), "withdrawn-members")


@app.route("/report/withdrawn-rdi-members.xlsx")
@auth_required
async def report_withdrawn_rdi_members(request, response):
    response.xlsx(reports.withdrawn_members(config.RDI_STR), "withdrawn-members")


@app.route("/report/full-eurordis-members-without-voting.xlsx")
@auth_required
async def report_full_eurordis_members_without_voting(request, response):
    response.xlsx(reports.full_members_without_voting(config.EURORDIS_STR), "full-members-without-voting")


@app.route("/report/eurordis-members-stats.xlsx")
@auth_required
async def eurordis_members_stats(request, response):
    response.xlsx(reports.members_stats(config.EURORDIS_STR), "members-stats-report")


@app.route("/report/rdi-members-stats.xlsx")
@auth_required
async def rdi_members_stats(request, response):
    response.xlsx(reports.members_stats(config.RDI_STR), "members-stats-report")


@app.route("/report/all-attendees.xlsx")
@auth_required
async def all_attendees(request, response):
    response.xlsx(reports.all_attendees(), "members-stats-report")


@app.route("/report/all-voting-contacts.xlsx")
@auth_required
async def all_voting_contacts(request, response):
    response.xlsx(reports.all_voting_contacts(), "voting-contacts-report")


@app.route("/report/representatives-stats.xlsx")
@auth_required
async def representatives_stats(request, response):
    response.xlsx(reports.representatives_stats(), "representatives-stats-report")


@app.route("/report/group-roles.xlsx")
@auth_required
async def dump_group_roles(request, response):
    response.xlsx(reports.role_to_group(), "group-roles.xlsx")


@app.route("/dump/event")
@auth_required
async def dump_events(request, response):
    response.xlsx(reports.events(), "dump-events.xlsx")


@app.route("/dump/group")
@auth_required
async def dump_groups(request, response):
    response.xlsx(reports.groups(), "dump-groups.xlsx")


@app.route("/dump/keyword")
@auth_required
async def dump_keywords(request, response):
    response.xlsx(reports.keywords(), "dump-keywords.xlsx")


@app.route("/dump/role")
@auth_required
async def dump_roles(request, response):
    response.xlsx(reports.dump_roles(), "dump-roles.xlsx")


@app.route("/dump/disease")
@auth_required
async def dump_diseases(request, response):
    response.csv(reports.diseases(), "diseases")


@app.route("/dump/organisation")
@auth_required
async def dump_organisation(request, response):
    response.xlsx(reports.organisations(), "dump-organisations.xlsx")


@app.route("/dump/{resource}")
@auth_required
async def dump(request, response, resource):
    klass = Query.resources.get(resource)
    if not klass:
        raise HttpError(400, f"Unknown resource {resource}")
    response.xlsx(reports.dump(klass), f"dump-{resource}.xlsx")


@app.route("/contact/mailchimp", methods=["POST"])
@auth_required
async def export_to_mailchimp(request, response):
    data = request.json
    resources = basket.load(data["resources"])
    response.json = await basket.export_as_mailchimp_segments(
        resources, data["name"], data["languages"]
    )


@app.route("/basket", methods=["POST"])
@auth_required
async def get_basket(request, response):
    response.json = {"data": basket.load(request.json["resources"])}


@app.route("/basket", methods=["PUT"])
@auth_required
async def populate_basket(request, response):
    resource = request.json.get("resource")
    cls = Organisation if resource == "organisation" else Person
    response.json = basket.from_refs(request.json["refs"], cls)


@app.route("/grouping/complete")
async def complete_grouping(request, response):
    q = request.query.get("q")
    limit = request.query.int("limit", 3)
    response.json = {"data": complete_grouping_(q, limit=limit)}


@app.route("/country/complete")
async def complete_country(request, response):
    q = request.query.get("q")
    limit = request.query.int("limit", 3)
    response.json = {"data": complete_country_(q, limit=limit)}


@app.route("/config")
async def get_config(request, response):
    def to_list(d):
        return [{"key": k, "value": v} for k, v in d.items()]

    EURORDIS = Organisation.get_organisation(config.EURORDIS_STR)

    response.json = {
        "GROUPING": to_list(constants.GROUPING),
        "TITLES": to_list(constants.TITLES),
        "LANGUAGES": to_list(constants.LANGUAGES),
        "ORGANISATION_KIND": to_list(constants.ORGANISATION_KIND),
        "MEMBERSHIP_STATUSES": to_list(constants.MEMBERSHIP_STATUSES),
        "GROUP_ROLES": constants.GROUP_ROLES,
        "PERSON_TO_GROUP_ROLES": constants.PERSON_TO_GROUP_ROLES,
        "ORGANISATION_TO_GROUP_ROLES": constants.ORGANISATION_TO_GROUP_ROLES,
        "EVENT_ROLES": constants.EVENT_ROLES,
        "EVENT_ROLE": constants.EVENT_ROLE,
        "EVENT_KINDS": to_list(constants.EVENT_KIND),
        "EVENT_KIND": to_list(constants.EVENT_KIND),
        "GROUP_KIND": to_list(constants.GROUP_KIND),
        "KEYWORD_KIND": to_list(constants.KEYWORD_KIND),
        "EURORDIS_ORGANISATION": EURORDIS.as_relation() if EURORDIS else None,
        "COUNTRIES": constants.COUNTRIES,
        "DISEASE_GROUP_OF_TYPES": constants.DISEASE_GROUP_OF_TYPES,
        "VERSION": VERSION,
    }


@app.route("/complete")
async def complete_all(request, response):
    q = request.query.get("q")
    limit = request.query.int("limit", 10)
    candidates = []
    if "@" in q:
        q = re.compile(f"^{q}", re.IGNORECASE)
        ids = list(Contact.objects(email=q).no_dereference().scalar("pk"))
        ids += [r.id for r in Role.objects(email=q).no_dereference().scalar("person")]
        candidates = [c.as_relation() for c in Contact.objects(pk__in=set(ids))]
        for candidate in candidates:
            candidate["score"] = 1
    elif q.startswith("http"):
        parsed = urlparse(q)
        q = re.compile(f"{parsed.netloc}", re.IGNORECASE)
        candidates = [c.as_relation() for c in Organisation.objects(website=q)]
        for candidate in candidates:
            candidate["score"] = 1
    elif ":" in q:
        # Smart search, do not try a real search.
        return candidates
    else:
        candidates.extend(helpers.complete_country(q, limit=3))
        keywords = [k.as_relation("labels") for k in Keyword.objects.search(q, limit)]
        candidates.extend(keywords)
        candidates.extend(helpers.complete_grouping(q, limit=3))
        candidates.extend(helpers.complete_kind(q, limit=3))
        candidates.extend(helpers.complete_event_kind(q, limit=3))
        candidates.extend(helpers.complete_event_role(q, limit=3))
        candidates.extend(helpers.complete_keyword_kind(q, limit=3))
        candidates.extend(helpers.complete_group_kind(q, limit=3))
        candidates.extend(helpers.complete_group_role(q, limit=3))
        groups = [g.as_relation("labels") for g in Group.objects.search(q, limit)]
        candidates.extend(groups)
        diseases = [d.as_relation("labels") for d in Disease.search(q, 3)]
        candidates.extend(diseases)
        # Search contacts with their role so we can get the role name for scoring.
        contacts = [c for c in Contact.objects.context_search(q, limit)]
        pks = [r["pk"] for r in contacts]
        insts = {r.pk: r.as_relation("labels") for r in Contact.objects(pk__in=pks)}
        for resource in contacts:
            resource.update(insts[resource["pk"]])
            role_name = (resource.get("role") or {}).get("name")
            if role_name:
                resource["labels"].append(role_name)
        candidates.extend(contacts)
        events = [c.as_relation("labels") for c in Event.objects.search(q, limit)]
        candidates.extend(events)
        for candidate in candidates:
            candidate["score"] = 0
            main_label = candidate["label"]
            labels = candidate.pop("labels", [candidate["label"]])
            for label in labels:
                score = helpers.compare_str(q, label)
                if label not in main_label:
                    score = round(score * 0.9, 2)
                if score > candidate["score"]:
                    candidate["score"] = score
                    if label not in main_label:
                        candidate["label"] = f"{main_label} ({label[:40]})"
                    else:  # Reset.
                        candidate["label"] = main_label
                if score >= 0.9:
                    break
        candidates = [c for c in candidates if c["score"] > 0.4]
        candidates.sort(key=lambda c: c["score"], reverse=True)
    response.json = {"data": candidates[:limit]}


# Deleted on Sept. 11 2022
# TODO: completely remove this endpoint on Mar. 11 2023 (6 months)
# @app.route("/eurordis-members")
# async def eurordis_members_list(request, response):
#     EURORDIS = Organisation.get_organisation(config.EURORDIS_STR)
#     data = []
#     for org in Organisation.organisation_members(EURORDIS):
#         org_data = org.as_relation("email", "website")
#         org_data["eurordis_status"] = org.organisation_membership_status(EURORDIS)
#         data.append(org_data)
#     response.json = {
#         "data": data
#     }


@app.route("/mailchimp-hook", methods=["GET", "POST"])
async def mailchimp_hook(request, response):
    if request.method == "GET":
        # MailChimp sends a GET to check that the webhook endpoint is active.
        return
    batch_errors = await mailchimp.on_batch(request.form)
    if not batch_errors:
        await after_batch()

@app.route("/activity", methods=["POST"])
@auth_required
async def log_activity(request, response):
    try:
        activity.log(**request.json)
    except ValueError:
        response.status = 422
        response.json = {"error": "Body must be valid json with keys: by, at, location"}
    else:
        response.status = 204


@app.route("/activity", methods=["GET"])
@auth_required
async def get_activity(request, response):
    response.json = {"data": activity.get(limit=request.query.int("limit", 1000))}


@app.route("/activity.csv", methods=["GET"])
@auth_required
async def report_activity(request, response):
    response.csv(activity.as_csv(), "activity")


@app.route("/search-activity.csv", methods=["GET"])
@auth_required
async def report_search_activity(request, response):
    response.csv(activity.search_as_csv(), "search-activity")


@app.route("/compare-groupings", methods=["POST"])
@auth_required
async def compare_grouping(request, response):
    response.csv(
        helpers.compare_grouping(request.files.get("data").read().decode()), "compare-groupings"
    )


@app.route("/voting-contacts")
@auth_required
async def voting_contacts(request, response):
    response.xlsx(reports.voting(), "voting-contacts")


@app.route("/send-receipt", methods=["POST"])
@auth_required(scope="admin")
async def send_receipt(request, response):
    data = request.json
    person_pk = data.get("person")
    org_pk = data.get("organisation")
    year = data.get("year")
    if not all((person_pk, org_pk, year)):
        raise HttpError("Body must contain person, organisation and year")
    contact = Role.get(person=person_pk, organisation=org_pk)
    try:
        receipt = receipts.fee(contact, year)
    except ValueError as err:
        raise HttpError(422, str(err))
    email = contact.get_email()
    if not email:
        raise HttpError(400, "No usable email found")
    body = emails.RECEIPT.format(person=contact.person, year=year)
    attachment = receipt()
    emails.send(
        email, "Eurordis Receipt", body, attachment, sender=config.FROM_EMAIL_RECEIPTS
    )
    attachment.seek(0)
    contact.organisation.add_attachment(attachment, receipt=year)
    contact.save()
    response.status = 204

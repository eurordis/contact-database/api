import io
import logging
import re
import sys
import tarfile
from datetime import date, timedelta
import csv

import ujson as json
from minicli import cli
from mongoengine.queryset.visitor import Q

from . import config, constants, emails, helpers
from .models import Contact, Event, Group, Keyword, Organisation, Person, Role

logger = logging.getLogger(__name__)
CHUNKS = 500  # Max allowed per call (MailChimp limit).


def get_base_url():
    prefix = config.MAILCHIMP_APIKEY.split("-")[1]
    return f"https://{prefix}.api.mailchimp.com/3.0/"


async def http(url, method, **kwargs):
    if not url.startswith("http"):
        url = f"{get_base_url()}{url}"
    return await helpers.http(
        url,
        method=method,
        auth=(config.MAILCHIMP_USER, config.MAILCHIMP_APIKEY),
        **kwargs,
    )


async def post(path, body):
    return await http(path, "POST", json=body)


async def patch(path, body):
    return await http(path, "PATCH", json=body)


async def get(path):
    return await http(path, "GET")


async def delete(path):
    return await http(path, "DELETE")


@cli
def hard_bounce_from_csv(file):
    try:
        keyword = Keyword.get(config.GDPR_HARDBOUNCE_KEYWORD)
    except Keyword.DoesNotExist:
        sys.exit("GDPR_HARDBOUNCE_KEYWORD invalid or not defined. Aborting.")
    with open(file) as f:
        rows = list(csv.reader(f))
    for row in rows[1:]:
        email = row[0]
        contact = Contact.objects(email=email).first()
        role = None
        if not contact:
            role = Role.objects(email=email).first()
            if role:
                contact = role.person
        if not contact:
            print(f"Can't find contact for email {email}")
            continue
        # hard bounce tag
        if keyword not in contact.keywords:
            contact.keywords.append(keyword)
        # remove email
        if role:
            role.email = None
        else:
            contact.email = None
        # add email in comment
        comment = contact.comment + "\n" if contact.comment else ""
        comment += f'"{email}" deleted after GDPR mailing'
        contact.comment = comment
        # save
        if role:
            role.save()
        contact.save()


@cli(name="hard-bounce-mailchimp")
async def hard_bounce(since=1):
    """Get hard bounce from Mailchimp and tag corresponding contacts in our database.

    :since: Ask only emails changed since the last x days.
    """
    if not config.MAILCHIMP_APIKEY:
        sys.exit("MAILCHIMP_APIKEY not set. Aborting.")
    try:
        keyword = Keyword.get(config.HARDBOUNCE_KEYWORD)
    except Keyword.DoesNotExist:
        sys.exit("HARDBOUNCE_KEYWORD invalid or not defined. Aborting.")
    today = date.today()
    since = today - timedelta(days=since)
    for name, props in get_sync_lists().items():
        list_id = props["id"]
        if not list_id:
            continue
        resp = await get(
            f"lists/{list_id}/members?status=cleaned&fields=total_items&since_last_changed={since.isoformat()}"
        )
        total = resp.json()["total_items"]
        count = 100
        members = []
        for offset in range(0, total, count):
            resp = await get(
                f"lists/{list_id}/members?status=cleaned&since_last_changed={since.isoformat()}&offset={offset}&count={count}"
            )
            data = resp.json()
            members.extend(data["members"])
        for member in members:
            email = member["email_address"]
            last_changed = member["last_changed"]
            contact = Contact.objects(email=email).first()
            if not contact:
                role = Role.objects(email=email).first()
                if role:
                    contact = role.person
            if not contact:
                print(f"Can't find contact for email {email}")
                continue
            if keyword in contact.keywords:
                print(f"Already tagged: {contact}")
                continue
            contact.keywords.append(keyword)
            comment = contact.comment + "\n" if contact.comment else ""
            comment += (
                f'Hard bounce on mailing list "{name}" on {last_changed[:10]}: {email}'
            )
            contact.comment = comment
            contact.save()
            print(f"Tagged: {contact}")


@cli(name="bootstrap-mailchimp")
async def bootstrap(force=False, webhook=None):
    """Synchronize merge field (MailChimp schema) for each list.

    :force: Force synchronization even if no new or old field is detected.
    :webhook: Also create a batch webhook with the given target url."""
    if not config.MAILCHIMP_APIKEY:
        sys.exit("MAILCHIMP_APIKEY not set. Aborting.")
    for name, props in get_sync_lists().items():
        await bootstrap_list(name, props, force)
    if webhook:
        print(f"Adding webhook {webhook}")
        await post("/batch-webhooks", body={"url": webhook})


async def bootstrap_list(name, props, force):
    print(f"Synchronizing merge fields for list `{name}`")
    list_id = props["id"]
    wanted = props["fields"]
    resp = await get(
        f"lists/{list_id}/merge-fields?count=99&fields=merge_fields.tag,merge_fields.merge_id"
    )
    actual = resp.json()["merge_fields"]
    for field in actual:
        if field["tag"] not in wanted:
            print(f"Removing field {field['tag']}")
            await delete(f"lists/{list_id}/merge-fields/{field['merge_id']}")
    actual = {f["tag"]: f for f in actual}
    for tag, field in wanted.items():
        if tag not in actual:
            print(f"Adding field {tag}")
            field["tag"] = tag
            await post(f"lists/{list_id}/merge-fields", body=field)
        elif force:
            # Remote has already the field, but it may have changed, let's sync.
            merge_id = actual[tag]["merge_id"]
            print(f"Force mode. Syncing field {tag}.")
            await patch(f"lists/{list_id}/merge-fields/{merge_id}", body=field)


async def sync_list(name, props, force=False):
    now = helpers.utcnow()
    list_id = props["id"]
    resp = await get(f"lists/{list_id}/members?fields=total_items")
    total = resp.json()["total_items"]
    wanted = props["getter"]()
    actual = []
    count = 100
    for offset in range(0, total, count):
        resp = await get(
            f"lists/{list_id}/members?fields=members.email_address,members.status,members.id&offset={offset}&count={count}"
        )
        actual.extend(resp.json()["members"])
    to_remove = []
    for member in actual:
        # MailChimp does what they want with case, so don't expect
        # them to respect the case of the email you send them.
        member["email_address"] = member["email_address"].lower()
        if member["email_address"] not in wanted and member["status"] == "subscribed":
            to_remove.append(member)
    if to_remove:
        print((await remove_members(list_id, to_remove)).json())
    new = []
    actual = {m["email_address"]: m for m in actual}
    for email, data in wanted.items():
        modified_at = data.pop("modified_at")
        if email and force or email not in actual or (now - modified_at) <= timedelta(hours=2):
            status = actual.get(email, {}).get("status", "subscribed")
            new.append(
                {"email_address": email, "status": status, "merge_fields": data}
            )
    if new:
        print((await add_members(list_id, new)).json())


@cli(name="sync-mailchimp")
async def sync(force=False, list=None):
    """Synchronize contacts in each list.

    :force: Force synchronization even if no new or old contact is detected."""
    if not config.MAILCHIMP_APIKEY:
        print(constants.MAILCHIMP_MISSING_KEY_ERROR)
        return
    for name, props in get_sync_lists().items():
        if list and list != name:
            print(f"Skipping list {name}")
            continue
        if not props["id"]:
            print(f"No key for list {name}. Skipping.")
            continue
        await sync_list(name, props, force=force)


async def add_members(list_id, members):
    print(f"Synchronizing {len(members)} members in list {list_id}")
    operations = []
    for i in range(0, len(members), CHUNKS):
        operations.append(
            {
                "method": "POST",
                "path": f"lists/{list_id}",
                "body": json.dumps(
                    {"members": members[i : i + CHUNKS], "update_existing": True}
                ),
            }
        )
    return await post("/batches", {"operations": operations})


async def remove_members(list_id, members):
    print(f"Removing {len(members)} members from list {list_id}")
    operations = []
    for member in members:
        operations.append(
            {"method": "DELETE", "path": f"lists/{list_id}/members/{member['id']}"}
        )
    return await post("/batches", {"operations": operations})


async def create_tag(name, emails):
    body = {"name": name, "static_segment": emails}
    await post(f"lists/{config.MAILCHIMP_LIST_EURORDIS_MEMBERS}/segments", body)


def get_rdi_members():
    RDI = Organisation.objects.get(pk=config.RDI_PK)
    rdi_members_orgs = Organisation.organisation_members(org_obj=RDI)
    rdi_keywords = Keyword.objects.filter(kind="rdi")
    rdi_keywords_orgs = Organisation.objects.filter(keywords__in=rdi_keywords)
    international_kind_orgs = Organisation.objects.filter(kind__in=[13, 63])
    rdi_orgs = list(
        set(
            list(rdi_members_orgs)
            + [RDI]
            + list(rdi_keywords_orgs)
            + list(international_kind_orgs)
        )
    )
    rdi_keywords_persons = Person.objects.filter(keywords__in=rdi_keywords)
    rdi_groups = Group.objects.filter(kind="rdi")
    rdi_events = Event.objects.filter(kind="rdi")
    roles = Role.objects.filter(
        Q(organisation__in=rdi_orgs)
        | Q(person__in=list(rdi_keywords_persons))
        | Q(groups__group__in=list(rdi_groups))
        | Q(events__event__in=list(rdi_events))
    )
    contacts = {}
    for role in roles:
        person, organisation = role.person, role.organisation
        email = role.email or person.email or organisation.email
        if not email:
            print(f"No email. Skipping {role}")
            continue
        firstlang = person.languages[0] if person.languages else "en"
        secondlang = person.languages[1] if len(person.languages) > 1 else ""
        status = ""
        groups = [g.name for g in person.groups]
        europe = person.country in constants.EUROPE_COUNTRIES
        for membership in organisation.membership:
            if membership.is_active_to(RDI):
                status = constants.MEMBERSHIP_STATUSES.get(membership.status, "")
                break
        if email in contacts and contacts[email]["MEMBSTATUS"]:
            # Contact is also related to an organisation member of RDI.
            # Let's give it precedence.
            continue
        contacts[email] = {
            "modified_at": max(person.modified_at, organisation.modified_at),
            "PK": person.pk,
            "FIRSTNAME": person.first_name,
            "LASTNAME": person.last_name,
            "TITLE": constants.TITLES.get(person.title, ""),
            "ORGNAME": organisation.name,
            "ORGID": organisation.pk,
            "MEMBSTATUS": status,
            "ORGCOUNTRY": organisation.country or "",
            "FIRSTLANG": firstlang,
            "SECONDLANG": secondlang,
            "CEF": "yes" if "CEF" in groups else "no",
            "CNA": "yes" if europe and "CNA" in groups else "no",
        }
    return contacts


def get_eurordis_members():
    EURORDIS = Organisation.objects.get(filemaker_pk=config.EURORDIS_FM_PK)
    org = Organisation.organisation_members(EURORDIS)
    roles = Role.objects.filter(organisation__in=list(org) + [EURORDIS])
    contacts = {}
    for role in roles:
        person, organisation = role.person, role.organisation
        email = role.email or person.email or organisation.email
        if not email:
            print(f"No email. Skipping {role}")
            continue
        firstlang = person.languages[0] if person.languages else "en"
        secondlang = person.languages[1] if len(person.languages) > 1 else ""
        status = ""
        groups = [g.name for g in person.groups]
        europe = person.country in constants.EUROPE_COUNTRIES
        for membership in organisation.membership:
            if membership.is_active_to(EURORDIS):
                status = constants.MEMBERSHIP_STATUSES.get(membership.status, "")
                break
        if email in contacts and contacts[email]["MEMBSTATUS"]:
            # Contact is also related to an organisation member of Eurordi.
            # Let's give it precedence.
            continue
        contacts[email] = {
            "modified_at": max(person.modified_at, organisation.modified_at),
            "PK": person.pk,
            "FIRSTNAME": person.first_name,
            "LASTNAME": person.last_name,
            "TITLE": constants.TITLES.get(person.title, ""),
            "ORGNAME": organisation.name,
            "ORGID": organisation.pk,
            "MEMBSTATUS": status,
            "ORGCOUNTRY": organisation.country or "",
            "FIRSTLANG": firstlang,
            "SECONDLANG": secondlang,
            "CEF": "yes" if "CEF" in groups else "no",
            "CNA": "yes" if europe and "CNA" in groups else "no",
        }
    return contacts


def get_test():
    contacts = {}
    try:
        test_keyword = Keyword.get(config.MAILCHIMP_TEST_KEYWORD)
    except Keyword.DoesNotExist:
        return contacts
    persons = Person.objects.filter(keywords=test_keyword.pk)
    for person in persons:
        email = person.get_email()
        if not email:
            print(f"No email. Skipping {person}")
            continue
        firstlang = person.languages[0] if person.languages else "en"
        secondlang = person.languages[1] if len(person.languages) > 1 else ""
        contacts[email] = {
            "modified_at": person.modified_at,
            "PK": person.pk,
            "FIRSTNAME": person.first_name,
            "LASTNAME": person.last_name,
            "COUNTRY": person.country or "",
            "FIRSTLANG": firstlang,
            "SECONDLANG": secondlang,
        }
    return contacts


def get_all():
    contacts = {}
    orgs = (
        Organisation.objects.filter(email__exists=True)
        .scalar("email", "name", "country", "modified_at")
        .no_dereference()
    )
    for email, name, country, modified_at in orgs:
        if not email:
            continue
        contacts[email] = {
            "modified_at": modified_at,
            "NAME": name,
            "COUNTRY": country or "",
            "LANG": helpers.lang_from_country(country),
            "FIRSTNAME": "",
            "TYPE": "organisation",
        }
    persons = (
        Person.objects.filter(email__exists=True)
        .scalar(
            "email", "first_name", "last_name", "country", "languages", "modified_at"
        )
        .no_dereference()
    )
    for email, first_name, last_name, country, languages, modified_at in persons:
        if not email:
            continue
        contacts[email] = {
            "modified_at": modified_at,
            "NAME": " ".join([first_name or "", last_name or ""]),
            "FIRSTNAME": first_name,
            "COUNTRY": country or "",
            "LANG": languages[0] if languages else helpers.lang_from_country(country),
            "TYPE": "person",
        }
    roles = Role.objects.filter(email__exists=True)
    for role in roles:
        if not role.email:
            continue
        contacts[role.email] = {
            "modified_at": role.person.modified_at,
            "NAME": role.person.label,
            "FIRSTNAME": role.person.first_name,
            "COUNTRY": role.person.country or "",
            "LANG": role.person.languages[0]
            if role.person.languages
            else helpers.lang_from_country(role.person.country),
            "TYPE": "person",
        }
    return contacts


def get_alumni():
    events = Event.objects(
        kind__in=[
            "digital_school",
            "leadership_school",
            "summer_school",
            "winter_school",
        ]
    ).scalar("pk")
    contacts = {}
    roles = Role.objects(
        __raw__={
            "events": {
                "$elemMatch": {
                    "event": {"$in": list(events)},
                    "role": re.compile("participant", re.IGNORECASE),
                },
            }
        }
    )
    for role in roles:
        email = role.get_email()
        if not email:
            continue
        contacts[email] = {
            "modified_at": role.person.modified_at,
            "NAME": str(role.person),
            "FIRSTNAME": role.person.first_name or "",
            "COUNTRY": role.person.country or "",
            "LANG": role.person.languages[0]
            if role.person.languages
            else helpers.lang_from_country(role.person.country),
        }
    return contacts


def get_sync_lists():
    return {
        "eurordis_members": {
            "id": config.MAILCHIMP_LIST_EURORDIS_MEMBERS,
            "getter": get_eurordis_members,
            "fields": {
                "PK": {"name": "PK", "type": "text"},
                "FIRSTNAME": {"name": "First Name", "type": "text"},
                "LASTNAME": {"name": "Last Name", "type": "text"},
                "TITLE": {"name": "Title", "type": "text"},
                "ORGNAME": {"name": "Organisation Name", "type": "text"},
                "ORGID": {"name": "Organisation ID", "type": "text"},
                "ORGCOUNTRY": {"name": "Organisation Country", "type": "text"},
                "MEMBSTATUS": {
                    "name": "Member Status",
                    "type": "radio",
                    "options": {
                        "choices": list(constants.MEMBERSHIP_STATUSES.values())
                    },
                },
                "FIRSTLANG": {"name": "First Language", "type": "text"},
                "SECONDLANG": {"name": "Second Language", "type": "text"},
                "CEF": {
                    "name": "CEF",
                    "type": "radio",
                    "options": {"choices": ["yes", "no"]},
                },
                "CNA": {
                    "name": "CNA Europe",
                    "type": "radio",
                    "options": {"choices": ["yes", "no"]},
                },
            },
        },
        "rdi_members": {
            "id": config.MAILCHIMP_LIST_RDI_MEMBERS,
            "getter": get_rdi_members,
            "fields": {
                "PK": {"name": "PK", "type": "text"},
                "FIRSTNAME": {"name": "First Name", "type": "text"},
                "LASTNAME": {"name": "Last Name", "type": "text"},
                "TITLE": {"name": "Title", "type": "text"},
                "ORGNAME": {"name": "Organisation Name", "type": "text"},
                "ORGID": {"name": "Organisation ID", "type": "text"},
                "ORGCOUNTRY": {"name": "Organisation Country", "type": "text"},
                "MEMBSTATUS": {
                    "name": "Member Status",
                    "type": "radio",
                    "options": {
                        "choices": list(constants.MEMBERSHIP_STATUSES.values())
                    },
                },
                "FIRSTLANG": {"name": "First Language", "type": "text"},
                "SECONDLANG": {"name": "Second Language", "type": "text"},
                "CEF": {
                    "name": "CEF",
                    "type": "radio",
                    "options": {"choices": ["yes", "no"]},
                },
                "CNA": {
                    "name": "CNA Europe",
                    "type": "radio",
                    "options": {"choices": ["yes", "no"]},
                },
            },
        },
        "test": {
            "id": config.MAILCHIMP_LIST_TEST,
            "getter": get_test,
            "fields": {
                "PK": {"name": "PK", "type": "text"},
                "FIRSTNAME": {"name": "First Name", "type": "text"},
                "LASTNAME": {"name": "Last Name", "type": "text"},
                "COUNTRY": {"name": "Country", "type": "text"},
                "FIRSTLANG": {"name": "First Language", "type": "text"},
                "SECONDLANG": {"name": "Second Language", "type": "text"},
            },
        },
        "all": {
            "id": config.MAILCHIMP_LIST_ALL,
            "getter": get_all,
            "fields": {
                "FIRSTNAME": {"name": "First Name", "type": "text"},
                "NAME": {"name": "Name", "type": "text"},
                "TYPE": {"name": "Type", "type": "text"},
                "COUNTRY": {"name": "Country", "type": "text"},
                "LANG": {"name": "Language", "type": "text"},
            },
        },
        "alumni": {
            "id": config.MAILCHIMP_LIST_ALUMNI,
            "getter": get_alumni,
            "fields": {
                "FIRSTNAME": {"name": "First Name", "type": "text"},
                "NAME": {"name": "Name", "type": "text"},
                "COUNTRY": {"name": "Country", "type": "text"},
                "LANG": {"name": "Language", "type": "text"},
            },
        },
    }


async def on_batch(data):
    # https://developer.mailchimp.com/documentation/mailchimp/guides/how-to-use-batch-operations/
    resp = await helpers.http(data["data[response_body_url]"][0], method="GET")
    if not resp.ok:
        emails.send(
            to=config.FROM_EMAIL,
            subject="MailChimp Batch Error",
            body="Can't retrieve zipfile",
        )
        return True
    tar = tarfile.open(fileobj=io.BytesIO(resp.content))
    for member in tar.getmembers():
        if member.name.endswith(".json"):
            f = tar.extractfile(member)
            if f is None:
                continue
            try:
                data = json.loads(f.read())
                data = data[0]["response"]
                data = json.loads(data)
            except Exception as err:
                logger.error(err)
                emails.send(
                    to=config.FROM_EMAIL,
                    subject="MailChimp Batch Error",
                    body="Can't load response status",
                )
                return True
            errors = data.get("errors") or []
            errors = "\n".join(f"{e['email_address']}: {e['error']}" for e in errors)
            if errors:
                emails.send(
                    to=config.FROM_EMAIL, subject="MailChimp Batch Error", body=errors
                )
                return True
    return False

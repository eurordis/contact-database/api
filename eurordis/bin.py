import csv
import json
import socket
import sys
import time
from datetime import timedelta
from pathlib import Path
from time import perf_counter
from zipfile import ZipFile

import pymongo
from minicli import cli, run, wrap
from mongoengine.connection import get_db
from progressist import ProgressBar
from roll.extensions import simple_server

from . import (
    activity,
    app,
    config,
    configure,
    constants,
    emails,
    helpers,
    jwt_token,
    receipts,
    reports,
    session,
)
from .loggers import logger
from .mailchimp import sync
from .mailchimp_monitoring import monitor_mailchimp_add_data
from .models import (
    Contact,
    Disease,
    Event,
    Group,
    Keyword,
    Organisation,
    Person,
    Role,
    RoleToEvent,
    Version,
)
from .orphadata import download_orphadata, import_orphadata, fix_lost_groupings  # NOQA
from .search.back import create_indexes as create_search_indexes, drop_collection


@cli
def create_indexes():
    drop_collection()
    create_search_indexes()
    for model in [Contact, Disease, Role, Version]:
        print(f"Creating indexes for {model.__name__}")
        model._get_collection().drop_indexes()
        # Create _id/_cls index
        model._get_collection().create_index(
            [("_id", pymongo.ASCENDING), ("_cls", pymongo.ASCENDING)], name="_id_cls"
        )
        for fields, options in model.indexes:
            name = model._get_collection().create_index(fields, **options)
            print(f"  Created index {name}")


@cli
def reindex():
    start = time.perf_counter()
    logger.info("Starting full reindex")
    create_indexes()
    for model in [Contact, Disease, Event, Keyword, Group]:
        qs = model.objects.all()
        bar = ProgressBar(prefix=f"Reindexing {model._class_name}", total=qs.count())
        for inst in bar.iter(qs):
            inst.index(cascade=True)
    logger.info("Full reindex done in %s", time.perf_counter() - start)


@cli
def serve(reload=False):
    """Run a web server (for development only)."""
    if reload:
        import hupper

        hupper.start_reloader("eurordis.bin.serve")
    simple_server(app)


@cli
def search_disease(q, limit=5, raw=False):
    """Test disease autocompletion."""
    print(f"Searching `{q}`")
    qs = Disease.objects.search(text=q, limit=limit, raw=raw)
    for i, obj in enumerate(qs):
        print(obj)
        disease = Disease.get(obj["_id"])
        print(
            f'{i+1}. {disease!r} => {obj["score"]} (found: {obj["found"]}, length: {obj["length"]})'
        )


@cli
def search(q, limit=5, raw=False):
    """Test contacts search."""
    print(f"Searching `{q}`")
    qs = Contact.objects.search(text=q, limit=limit, raw=raw)
    for i, obj in enumerate(qs):
        if raw:
            # print(obj)
            contact = Contact._from_son(obj["obj"])
            print(
                f'{i+1}. {contact!r} => {obj["score"]} (found: {obj["found"]}, length: {obj["length"]})'
            )
        else:
            print(obj)


@cli
def migrate_grouping():
    rows = list(Disease._get_collection().find({"erns": {"$nin": [None, []]}}))
    bar = ProgressBar(prefix="Migrating erns to grouping", total=len(rows))
    for raw in bar.iter(rows):
        raw["groupings"] = [
            {"name": e["ern"], "validated": e.get("validated")}
            for e in raw.pop("erns")
        ]
        disease = Disease._from_son(raw)
        disease._changed_fields.append("groupings")
        disease.save(cascade=True)


@cli
def clean_db(reset_diseases=False):
    """For development convenience, clean all models in database."""
    hostname = socket.gethostname()
    if input(f"Type the hostname ({hostname}): ") != hostname or input("Are you sure to clean the database, removing all contacts and organisations? [y/N] ").lower().strip() != "y":
        sys.exit("Aborted. No data changed.")
    print("Deleting all contacts and organisations...")
    Role.drop_collection()
    Organisation.drop_collection()
    Person.drop_collection()
    Event.drop_collection()
    Version.drop_collection()
    if reset_diseases:
        Disease.drop_collection()
    # Reset counters
    get_db().get_collection("mongoengine.counters").drop()
    print("Deleted!")


@cli
def shell():
    """Run an ipython already connected to Mongo."""
    try:
        from IPython import start_ipython
    except ImportError:
        print('IPython is not installed. Type "pip install ipython"')
    else:
        start_ipython(
            argv=[],
            user_ns={
                "Contact": Contact,
                "Disease": Disease,
                "Person": Person,
                "Organisation": Organisation,
                "Event": Event,
                "session": session,
                "constants": constants,
                "Group": Group,
                "Keyword": Keyword,
                "Version": Version,
                "Role": Role,
            },
        )


@cli
def create_admin_user(email):
    try:
        person = Person.objects.get(email=email)
    except Person.DoesNotExist:
        person = Person(email=email, first_name="Cesaria", last_name="Evora").save()
    token, _ = jwt_token(person, person.email)
    print(f"You can now use `{person}` with token {token}")


@cli
def hard_delete(dry_run=False):
    """Delete for real objects marked as 'To be deleted'"""
    if not config.TOBEDELETED_KEYWORD:
        sys.exit("TOBEDELETED_KEYWORD not defined. Aborting")
    bound = helpers.utcnow() - timedelta(days=config.SOFT_DELETE_QUARANTINE)
    contacts = Contact.objects(
        keywords=config.TOBEDELETED_KEYWORD, modified_at__lt=bound
    )
    print(f"{len(contacts)} contacts to delete.")
    if dry_run:
        print("Dry run. Aborting.")
        sys.exit()
    for contact in contacts:
        try:
            contact.delete()
        except ValueError:
            # Logme?
            continue


@cli
def migrate_events():
    """Migrate events from persons to roles."""
    qs = Person._get_collection().find(
        {
            "$and": [
                {"events": {"$exists": True}},
                {"events": {"$nin": [[], None]}},
                {"_cls": "Contact.Person"},
            ]
        },
        {"events": True},
    )
    bar = ProgressBar(prefix="Migrating persons", total=qs.count())
    no_default = []
    for raw in bar.iter(qs):
        person = Person.get(raw["_id"])
        role = person.default_role
        if not role:
            no_default.append(person)
            continue
        role.events = [RoleToEvent(**e) for e in raw["events"]]
        role.save()
    print("No default:")
    print(no_default)


@cli
def history(limit=10):
    """Display last changes in the console."""

    def print_sub(sub):
        if isinstance(sub, list):
            for ssub in sub:
                print_sub(ssub)
        if "subject" in sub:
            print(
                " " * 5,
                sub["subject"],
                sub["verb"],
                sub.get("old", ""),
                sub.get("new", ""),
            )
        if "subordinates" in sub:
            print_sub(sub["subordinates"])

    for stage in Version.history(limit=limit):
        print("* ", stage.of["label"], f"(by {stage.by}, at {stage.at})")
        for key, subordinates in stage.diff.items():
            print("     -", key)
            print_sub(subordinates)


@cli
def dump_disease(path: Path):
    path.write_text(reports.diseases())


@cli
def import_extra_diseases():
    with (config.ROOT / "data/extra.csv").open() as f:
        rows = list(csv.reader(f, delimiter=";"))
    NON_RARE = Disease.get(config.NON_RARE_ORPHA_NUMBER)
    new = set([int(r[0]) for r in rows])
    old = set(Disease.objects(roots=NON_RARE).scalar("orpha_number"))
    to_remove = old - new
    for pk in to_remove:
        disease = Disease.get(pk)
        if not Contact.objects(diseases=disease):
            print(f"Deleting {disease}")
            disease.delete()
        else:
            print(f"Cannot delete {disease}, still linked to contacts")
    for pk, name, status in rows:
        try:
            disease = Disease.get(pk)
        except Disease.DoesNotExist:
            disease = Disease(
                name=name,
                orpha_number=pk,
                roots=[NON_RARE],
                ancestors=[NON_RARE],
                status=status,
            ).save()
            NON_RARE.descendants.append(disease)
            print(f"Created {disease}")
        else:
            if name != disease.name or status != disease.status:
                disease.name = name
                disease.status = status
                disease.save()
                print(f"Updated {disease}")
            else:
                print(f"Already in the DB: {disease}")
    NON_RARE.save()


@cli
def dump_reports():
    with ZipFile("/tmp/eurordis-reports.zip", mode="w") as z:
        z.writestr("activity.csv", activity.as_csv())
        z.writestr("search-activity.csv", activity.search_as_csv())
        z.writestr("contact-database-history.csv", reports.history())
        z.writestr("disease.csv", reports.diseases())

@cli
def chore_report(email=[]):
    DOMAIN = "https://contact-database.eurordis.local"
    EURORDIS = Organisation.get_organisation(config.EURORDIS_STR)
    members = set(Organisation.organisation_members(EURORDIS).no_dereference().scalar("pk"))
    full_members = set(
        Organisation.organisation_members(EURORDIS, status=["f"]).no_dereference().scalar("pk")
    )
    primary = {
        ref.id
        for ref in Role.objects(is_primary=True).no_dereference().scalar("organisation")
    }
    forsaken = Organisation.objects(pk__in=members - primary)
    forsaken = [f"- {org}\n  {DOMAIN}/organisation/{org.pk}" for org in forsaken]
    forsaken = "\n".join(forsaken)
    voting = {
        ref.id
        for ref in Role.objects(is_voting=True).no_dereference().scalar("organisation")
    }
    no_voting = Organisation.objects(pk__in=full_members - voting)
    no_voting = [f"- {org}\n  {DOMAIN}/organisation/{org.pk}" for org in no_voting]
    no_voting = "\n".join(no_voting)
    duplicates = Role.objects.aggregate(
        {"$match": {"email": {"$ne": None}}},
        {"$group": {"_id": "$email", "count": {"$sum": 1}}},
        {"$match": {"count": {"$gt": 1}}},
        {"$project": {"email": "$_id", "_id": 0}},
    )
    duplicates = [d["email"] for d in duplicates]
    in_roles = "\n".join(f"- {e}\n  {DOMAIN}/?q={e}" for e in duplicates)
    values = Role.objects(email__ne=None).no_dereference().scalar("email")
    in_contacts = Contact.objects(email__in=values)
    in_contacts = "\n".join(
        f"- {c.email}\n  {DOMAIN}/{c.resource}/{c.pk}" for c in in_contacts
    )
    pks = Contact.objects(diseases__nin=[[], None]).no_dereference().scalar("diseases")
    pks = set(ref.id for refs in pks for ref in refs)
    no_groupings = Disease.objects(pk__in=pks, groupings=[])
    no_groupings = "\n".join(f"- {d} {DOMAIN}/disease/{d.pk}" for d in no_groupings)
    # Person with multiple organisations but no default one.
    raw = Role._get_collection().aggregate(
        [
            {
                "$group": {
                    "_id": "$person",
                    "orgs": {"$push": "$organisation"},
                    "is_default": {"$push": "$is_default"},
                    "is_archived": {"$push": "$is_archived"},
                }
            },
            {"$project": {"size": {"$size": "$orgs"}, "is_default": "$is_default", "is_archived": "$is_archived"}},
            {"$match": {"size": {"$gt": 1}, "is_default": {"$nin": [True]}, "is_archived": {"$ne": True}}},
        ]
    )

    ids = [s["_id"] for s in raw]
    persons = Person.objects(pk__in=ids)
    no_default = [f"- {person}\n  {DOMAIN}/person/{person.pk}" for person in persons]
    no_default = "\n".join(no_default)
    # Person with a single organisation and set as default.
    raw = Role._get_collection().aggregate(
        [
            {
                "$group": {
                    "_id": "$person",
                    "orgs": {"$push": "$organisation"},
                    "is_default": {"$push": "$is_default"},
                }
            },
            {"$project": {"size": {"$size": "$orgs"}, "is_default": "$is_default"}},
            {"$match": {"size": 1, "is_default": [True]}},
        ]
    )
    ids = [s["_id"] for s in raw]
    persons = Person.objects(pk__in=ids)
    single_default = [
        f"- {person}\n  {DOMAIN}/person/{person.pk}" for person in persons
    ]
    single_default = "\n".join(single_default)
    body = emails.STAFF_REPORT.format(
        forsaken=forsaken or None,
        no_voting=no_voting or None,
        in_roles=in_roles or None,
        in_contacts=in_contacts or None,
        no_groupings=no_groupings or None,
        no_default=no_default or None,
        single_default=single_default or None,
    )
    emails.send(to=email or config.SEND_REPORT_EMAILS, subject="Contact Database Report", body=body)



@cli
def epag_members(path: Path):
    data = reports.epag_members()
    path.write_text(json.dumps(data, ensure_ascii=False, indent=2))
    print(f"Saved to {path}")


@cli
def migrate_corporate_email():
    persons = list(Person.objects(email__ne=None))
    bar = ProgressBar(prefix="Processing", total=len(persons))
    for person in bar.iter(persons):
        domain = person.email.split("@")[1]
        if domain in constants.GENERIC_EMAILS:
            continue
        for role in person.roles:
            email = role.organisation.email or ""
            website = role.organisation.website or ""
            if email.endswith(domain) or domain in website:
                role.email = person.email
                role.save()
                person.email = None
                person.save()
                break


@cli
def token(pk=1, email=None):
    person = Person.get(pk)
    print(f"Token for {person}")
    token, exp = jwt_token(person, email or person.email)
    print(token.decode())


@cli
def receipt(person=9400, organisation=3176, year=2020):
    role = Role.get(person=person, organisation=organisation)
    pdf = receipts.fee(role, year)
    pdf("tmp/receipt.pdf")


@wrap
def cli_wrapper():
    configure()
    start = perf_counter()
    with session.bot():
        yield
    elapsed = perf_counter() - start
    print(f"Done in {elapsed:.5f} seconds.")


def main():
    if config.READONLY:
        sys.exit("contact database is readonly")
    run()

@cli(name="monitor-mailchimp")
async def monitor_mailchimp():
    monitor_mailchimp_add_data()
    # TODO: remove useless code below after entirely migrated to Mailchimp hook endpoint
    # try:
    #     failed = []
    #     # Get testing data
    #     event, role, organisation, person = monitor_mailchimp_add_data()
    #     # Sync lists
    #     await sync()
    #     # Verify lists
    #     failed_added_verification = await monitor_mailchimp_verify(person.email,"add a user")
    #     failed.extend(failed_added_verification)
    #     # Update email
    #     previous_email = person.email
    #     person.email = monitor_mailchimp_email()
    #     person.save()
    #     # Sync lists
    #     await sync()
    #     # Verify lists
    #     failed_updated_verification = await monitor_mailchimp_verify(previous_email, "delete a user's previous email", deleted=True)
    #     failed.extend(failed_updated_verification)
    #     failed_updated_verification = await monitor_mailchimp_verify(person.email, "add a user's new email")
    #     failed.extend(failed_updated_verification)
    #     # Delete testing data
    #     monitor_mailchimp_delete_data(event, role, organisation, person)
    #     # Sync lists
    #     await sync()
    #     # Verify lists
    #     failed_deleted_verification = await monitor_mailchimp_verify(person.email, "delete a user", deleted=True)
    #     failed.extend(failed_deleted_verification)
    #     # Send alerts
    #     if failed:
    #         messages = "\n".join(failed)
    #         body = emails.MAILCHIMP_REPORT.format(messages=messages)
    #         emails.send(to=config.MAILCHIMP_REPORT_EMAILS, subject="Contact Database Mailchimp Report", body=body)
    # finally:
    #     monitor_mailchimp_delete_data(event, role, organisation, person)

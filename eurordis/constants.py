from . import config

data = [
    l.split(";")
    for l in (config.ROOT / "data/countries.csv").read_text().splitlines()
    if l
]

COUNTRIES = {code: name for code, name, *_ in data}
COUNTRIES_REVERSED = {}
for line in data:
    code, *names = line
    for name in names:
        if name:
            COUNTRIES_REVERSED[name] = code
COUNTRIES_AND_AREAS = {**COUNTRIES, "EU": "European Union", "EUR": "Europe"}


# fmt: off
EU_COUNTRIES = [
    "AT", "BE", "BG", "CY", "CZ", "DK", "EE", "FI", "FR", "DE", "GR", "HR", "HU", "IE",
    "IT", "LV", "LT", "LU", "MT", "NL", "PL", "PT", "RO", "SK", "SI", "ES", "SE",
]
EUROPE_COUNTRIES = EU_COUNTRIES + [
    "AD", "AL", "AM", "AZ", "BA", "BY", "CH", "GB", "GE", "IS", "LI", "MC",
    "MD", "ME", "MK", "NO", "RS", "RU", "SM", "TR", "UA",
]
# fmt: on
NON_EU_COUNTRIES = [c for c in COUNTRIES if c not in EU_COUNTRIES]
NON_EUROPE_COUNTRIES = [c for c in COUNTRIES if c not in EUROPE_COUNTRIES]

# keys from https://ec.europa.eu/health/ern/networks_en
GROUPING = {
    "bond": "Bone",
    "cranio": "Cranio-facial & ENT",
    "endo": "Endocrine",
    "epicare": "Epilepsy",
    "erknet": "Renal",
    "ernica": "Gastrointestinal",
    "euracan": "Adult Solid Cancer",
    "eurobloodnet": "Haematological",
    "eurogen": "Urogenital",
    "euronmd": "Neuromuscular",
    "eye": "Eye",
    "genturis": "Genetic Tumor Risk",
    "guardheart": "Cardiac",
    "ithaca": "Malformations/dev. anomalies/intellectual disab",
    "lung": "Pulmonary",
    "metab": "Metabolic",
    "paedcan": "Paediatric cancer",
    "rareliver": "Hepatic",
    "reconnet": "Connective tissue & musculoskeletal",
    "rita": "Immunological & autoinflammatory",
    "rnd": "Neurological",
    "skin": "Skin",
    "transplantchild": "Transplant",
    "vasc": "Vascular",
}

# tens are categories, units are subcategories.
ORGANISATION_KIND = {
    10: "Public Body",
    11: "European Union Public Body",  # --> name like '%european commission%', '%european union%'
    12: "National Public Body",  # --> name like '%minist%'
    13: "International Public Body",
    20: "Regulator",
    100: "Research institute / university",
    30: "Healthcare Provider (HCP)",  # --> name like '%hospital%','%hôpital%'
    110: "Social care provider",
    40: "Corporation",
    41: "Pharmaceutical and Biotech Company",  # --> is_pharmaceutical_company = 1
    42: "Other Health Company",
    43: "Outside Health Company",
    50: "Not for Profit Organisation (NPO)",
    51: "Pharmaceutical and Biotech NPO",
    52: "Other Health NPO",
    53: "Outside Health NPO",
    60: "Patient Organisation",  # --> _patient_organisation = 1
    61: "European Federation",  # --> is_federation = 1
    62: "National Alliance",  # --> is_national_alliance = 1
    63: "International Federation",  # --> _patient_organisation = 1 AND name like '%international%'
    70: "Learned Society",
    80: "Press",  # (will be used only if they can be managed appart) --> keyword = Press
    90: "Supplier",  # (will be used only if they can be managed appart) --> keyword = Supplier
}

TITLES = {"mr": "Mr", "ms": "Ms", "pr": "Pr", "dr": "Dr"}

LANGUAGES = {
    "fr": "French",
    "en": "English",
    "de": "German",
    "it": "Italian",
    "es": "Spanish",
    "pt": "Portuguese",
    "ru": "Russian",
}

GROUP_ROLES = [
    "Member",
    "Alternate",
    "Applicant",
    "Chair",
    "Contact",
    "Coordinator",
    "Director",
    "Member",
    "Moderator",
    "Observer",
    "Officer",
    "Patient Representative",
    "Project Manager",
    "Staff",
    "Supporting Partner"
]
PERSON_TO_GROUP_ROLES = GROUP_ROLES
ORGANISATION_TO_GROUP_ROLES = GROUP_ROLES

EVENT_ROLE = [
    "Awardee",
    "Chair",
    "Delete",
    "Exhibitor",
    "Fellow",
    "Moderator",
    "Panelist",
    "Participant",
    "Speaker",
    "Staff",
    "Volunteer",
]

EVENT_ROLES = EVENT_ROLE

EVENT_KIND = {
    "awards": "Awards ceremony",
    "cef": "CEF",
    "cna": "CNA",
    "digital_school": "Digital School",
    "ecrd": "ECRD - European Conference on Rare Diseases",
    "emm": "EMM - EURORDIS Membership Meeting",
    "epag": "ePAG",
    "ertc": "ERTC",
    "international": "International",
    "leadership_school": "Leadership School",
    "project": "Project",
    "rdd": "Rare Disease Day",
    "rdi": "Rare Diseases International",
    "rdw": "Rare Disease Week",
    "summer_school": "Summer School",
    "winter_school": "Winter School",
    "webinar": "Webinar",
}

DISEASE_GROUP_OF_TYPES = {
    "group": "Group of disorders",
    "disorder": "Disorder",
    "subtype": "Subtype of disorder",
}

MEMBERSHIP_STATUSES = {"f": "Full", "a": "Associate", "w": "Withdrawn"}

COUNTRY_TO_LANG = {
    "AR": "es",
    "BF": "fr",
    "BI": "fr",
    "BJ": "fr",
    "BO": "es",
    "BR": "pt",
    "CF": "fr",
    "CG": "fr",
    "CL": "es",
    "CM": "fr",
    "CO": "es",
    "CR": "es",
    "CU": "es",
    "DJ": "fr",
    "DM": "fr",
    "DO": "es",
    "EC": "es",
    "ES": "es",
    "FR": "fr",
    "GA": "fr",
    "GN": "fr",
    "GQ": "es",
    "GT": "es",
    "HN": "es",
    "HT": "fr",
    "IT": "it",
    "KM": "fr",
    "LC": "fr",
    "LU": "fr",
    "MA": "fr",
    "MC": "fr",
    "MG": "fr",
    "ML": "fr",
    "MX": "es",
    "NE": "fr",
    "NI": "es",
    "PA": "es",
    "PE": "es",
    "PT": "pt",
    "PY": "es",
    "RW": "fr",
    "SC": "fr",
    "SN": "fr",
    "SV": "es",
    "TD": "fr",
    "TG": "fr",
    "UY": "es",
    "VE": "es",
    "VU": "fr",
}

GROUP_KIND = {
    # @TODO: `epag` should probably renamed to `ern`
    "epag": "ERN - European Reference Network",
    "ec": "EC project",
    "ema": "EMA Committee",
    "political": "Political party",
    "internal": "Internal Group / Task Force",
    "rdi": "Rare Diseases International",
    "eu": "European Union",
    "transversal": "ePAG Transversal Groups"
}

KEYWORD_KIND = {
    "expertise": "Expertise",
    "membership_update": "Membership Update",
    "rdi": "Rare Diseases International",
    "chore": "Contact database process",
}


GENERIC_EMAILS = [
    "abv.bg",
    "aol.com",
    "btinternet.com",
    "cegetel.net",
    "club-internet.fr",
    "cytanet.com.cy",
    "eircom.net",
    "free.fr",
    "gmail.com",
    "gmx.at",
    "gmx.de",
    "hotmail.co.uk",
    "hotmail.com",
    "hotmail.fr",
    "laposte.net",
    "libero.it",
    "mail.dk",
    "mail.ru",
    "me.com",
    "mweb.co.za",
    "netvision.net",
    "netvision.net.il",
    "orange.com",
    "orange.fr",
    "outlook.com",
    "outlook.it",
    "pandora.be",
    "seznam.cz",
    "skynet.be",
    "t-online.de",
    "telia.com",
    "terra.es",
    "tin.it",
    "tiscali.it",
    "wanadoo.fr",
    "web.de",
    "yahoo.co.uk",
    "yahoo.com",
    "yahoo.es",
    "yahoo.fr",
    "yahoo.it",
    "ziggo.nl",
]

MAILCHIMP_MISSING_KEY_ERROR = "Missing MailChimp APIKEY. Aborting."
MAILCHIMP_MONITORING_MEMBER_ORGANISATION = "TEMPORARY TEST ORGANISATION [DO NOT TOUCH]"
MAILCHIMP_MONITORING_EVENT = "TEMPORARY TEST EVENT [DO NOT TOUCH]"
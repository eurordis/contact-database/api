import { Document, MongoClient, WithId } from "mongodb";
import { faker } from "@faker-js/faker";
import { sourceUri, targetUri } from "./config.json";

const ANONYMIZE_VARIABLES = [
  "name",
  "acronym",
  "first_name",
  "last_name",
  "email",
  "phone",
  "cell_phone",
  "address",
  "country",
  "postcode",
  "city",
  "website",
];

function anonymizeValue(
  collectionName: string,
  key: string,
  documentData: WithId<Document>
) {
  if (
    key === "name" &&
    collectionName === "contact" &&
    documentData._cls === "Contact.Organisation" &&
    documentData._id.toString() !== "1554" &&
    documentData._id.toString() !== "4524"
  ) {
    return faker.lorem.words({ min: 1, max: 3 });
  } else if (
    key === "acronym" &&
    collectionName === "contact" &&
    documentData._cls === "Contact.Organisation" &&
    documentData._id.toString() !== "1554" &&
    documentData._id.toString() !== "4524"
  ) {
    return faker.lorem.word({ length: 3 });
  } else if (key === "first_name") {
    return faker.person.firstName();
  } else if (key === "last_name") {
    return faker.person.lastName();
  } else if (
    key === "email" &&
    documentData.email !== "contact-database@eurordis.org"
  ) {
    return faker.internet.email();
  } else if (key === "phone" || key === "cell_phone") {
    return faker.phone.number();
  } else if (key === "address") {
    return faker.location.streetAddress();
  } else if (key === "country") {
    return faker.location.countryCode().toLowerCase();
  } else if (key === "postcode") {
    return faker.location.zipCode();
  } else if (key === "city") {
    return faker.location.city();
  } else if (key === "website") {
    return "https://example.com/";
  } else {
    return documentData[key];
  }
}

function anonymizeDocument(
  collectionName: string,
  documentData: WithId<Document>
) {
  for (const key in documentData) {
    if (ANONYMIZE_VARIABLES.includes(key)) {
      documentData[key] = anonymizeValue(collectionName, key, documentData);
    }
  }

  return documentData;
}

(async function main() {
  console.log("Connecting to source...");
  const client = new MongoClient(sourceUri);
  await client.connect();
  const db = client.db();

  console.log("Connecting to target...");
  const targetClient = new MongoClient(targetUri);
  await targetClient.connect();
  const targetDb = targetClient.db();

  console.log("Fetching collections...");
  const collections = await db.listCollections().toArray();
  console.log("Collections: " + collections.map((item) => item.name));

  console.log("Anonymizing collections...");
  for (const collection of collections) {
    const collectionName = collection.name;

    console.log("Anonymizing collection: " + collectionName);
    const collectionData = await db.collection(collectionName).find().toArray();
    const collectionDataAnonymized = collectionData.map((documentData) =>
      anonymizeDocument(collectionName, documentData)
    );

    console.log("Inserting collection in target: " + collectionName);
    await targetDb.collection(collectionName).deleteMany({});
    // Save collection
    await targetDb
      .collection(collectionName)
      .insertMany(collectionDataAnonymized);
  }
  console.log("Done!");

  await client.close();
  await targetClient.close();
})();

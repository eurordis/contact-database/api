from io import BytesIO
from pathlib import Path

import pytest
from eurordis import config, importer
from eurordis.models import Organisation, Person, Role
from openpyxl import Workbook


@pytest.fixture
def workbook():
    def _(rows, headers=["first_name", "last_name", "email"]):
        wb = Workbook()
        ws = wb.active
        ws.append(headers)
        for row in rows:
            ws.append(row)
        return wb

    return _


@pytest.fixture(autouse=True, scope="function")
def module_setup_teardown(keyword):
    config.TOBEREVIEWED_KEYWORD = keyword.pk


def test_mandatory_headers(workbook):
    with pytest.raises(ValueError):
        importer.process(
            workbook(
                [("Bob", "Dylan", "bob@dylan.com")],
                headers=["city", "country", "email"],
            )
        )


def test_bad_file(workbook):
    with pytest.raises(ValueError):
        importer.process(BytesIO(b"pouet"))


def test_simple_import(workbook, person, keyword):
    assert Person.objects(email="bob@dylan.com").count() == 0
    # With an empty row.
    report, persons, _ = importer.process(
        workbook([("Bob", "Dylan", "bob@dylan.com"), (None, None, None)])
    )
    assert Person.objects(email="bob@dylan.com").count() == 1
    assert len(persons) == 1
    person = persons[0]
    assert person.keywords == [keyword]
    assert report == [
        {
            "data": {
                "country": None,
                "email": "bob@dylan.com",
                "first_name": "Bob",
                "label": "Bob Dylan",
                "last_name": "Dylan",
                "pk": person.pk,
                "resource": "person",
                "uri": f"/person/{person.pk}",
            },
            "status": "created",
            "report": {},
            "raw": {
                "email": "bob@dylan.com",
                "first_name": "Bob",
                "last_name": "Dylan",
            },
        }
    ]


def test_should_not_create_duplicate_email(workbook, person):
    report, persons, _ = importer.process(
        workbook([(person.first_name, person.last_name, person.email.upper())])
    )
    assert Person.objects(email=person.email).count() == 1
    assert len(persons) == 1
    assert report == [
        {
            "data": {
                "country": "FR",
                "email": "bill@somewhere.org",
                "first_name": "Bill",
                "label": "Bill Mollison",
                "last_name": "Mollison",
                "pk": person.pk,
                "resource": "person",
                "uri": f"/person/{person.pk}",
            },
            "status": "found",
            "report": {},
            "raw": {
                "email": "BILL@SOMEWHERE.ORG",
                "first_name": "Bill",
                "last_name": "Mollison",
            },
        }
    ]


def test_should_tolerate_first_or_last_name_different(workbook, person):
    report, persons, _ = importer.process(
        workbook([("william", person.last_name.lower(), person.email)])
    )
    assert Person.objects(email=person.email).count() == 1
    assert len(persons) == 1
    assert report == [
        {
            "data": {
                "country": "FR",
                "email": "bill@somewhere.org",
                "first_name": "Bill",
                "label": "Bill Mollison",
                "last_name": "Mollison",
                "pk": person.pk,
                "resource": "person",
                "uri": f"/person/{person.pk}",
            },
            "status": "found",
            "report": {"first_name": "Searched 'william', found 'Bill'"},
            "raw": {
                "email": "bill@somewhere.org",
                "first_name": "william",
                "last_name": "mollison",
            },
        }
    ]


def test_should_tolerate_first_and_last_names_different(workbook, person):
    report, persons, _ = importer.process(workbook([("Someone", "Else", person.email)]))
    assert Person.objects(email=person.email).count() == 1
    assert Person.objects.get(email=person.email).first_name == person.first_name
    assert Person.objects.get(email=person.email).last_name == person.last_name
    assert persons[0].email == person.email
    assert len(persons) == 1
    assert report == [
        {
            "data": {
                "pk": "1",
                "first_name": "Bill",
                "last_name": "Mollison",
                "email": "bill@somewhere.org",
                "uri": "/person/1",
                "resource": "person",
                "label": "Bill Mollison",
                "country": "FR",
            },
            "status": "found",
            "report": {
                "first_name": "Searched 'Someone', found 'Bill'",
                "last_name": "Searched 'Else', found 'Mollison'",
            },
            "raw": {
                "email": "bill@somewhere.org",
                "first_name": "Someone",
                "last_name": "Else",
            },
        }
    ]


def test_should_match_first_name_and_last_name_if_found_has_no_email(workbook, person):
    person.email = None
    person.save()
    # Case insensitive.
    report, persons, _ = importer.process(
        workbook([(person.first_name.lower(), person.last_name, "other@email.org")])
    )
    assert len(persons) == 1
    assert report == [
        {
            "data": {
                "country": "FR",
                "email": "other@email.org",
                "first_name": "Bill",
                "label": "Bill Mollison",
                "last_name": "Mollison",
                "pk": person.pk,
                "resource": "person",
                "uri": f"/person/{person.pk}",
            },
            "status": "found",
            "report": {"email": "Attached email `other@email.org`"},
            "raw": {
                "email": "other@email.org",
                "first_name": "bill",
                "last_name": "Mollison",
            },
        }
    ]
    assert Person.get(id=person.pk).email == "other@email.org"


def test_should_match_first_name_and_last_name_if_found_has_an_email(workbook, person):
    report, persons, _ = importer.process(
        workbook([(person.first_name.lower(), person.last_name, "other@email.org")])
    )
    assert len(persons) == 1
    assert report == [
        {
            "data": {
                "country": "FR",
                "email": "bill@somewhere.org",
                "first_name": "Bill",
                "label": "Bill Mollison",
                "last_name": "Mollison",
                "pk": person.pk,
                "resource": "person",
                "uri": f"/person/{person.pk}",
            },
            "status": "found",
            "report": {
                "email": "User `Bill Mollison` has another email in DB: `bill@somewhere.org`",
            },
            "raw": {
                "email": "other@email.org",
                "first_name": "bill",
                "last_name": "Mollison",
            },
        }
    ]
    assert Person.get(id=person.pk).email == "bill@somewhere.org"


def test_should_not_duplicate_role_email_on_name_match(workbook, person, organisation):
    role = Role.objects.get(person=person, organisation=organisation)
    report, persons, _ = importer.process(
        workbook([(person.first_name.lower(), person.last_name, role.email)])
    )
    assert len(persons) == 1
    assert report == [
        {
            "data": {
                "country": "FR",
                "email": "bill@somewhere.org",
                "first_name": "Bill",
                "label": "Bill Mollison",
                "last_name": "Mollison",
                "pk": person.pk,
                "resource": "person",
                "uri": f"/person/{person.pk}",
            },
            "status": "found",
            "report": {},
            "raw": {
                "email": role.email,
                "first_name": "bill",
                "last_name": "Mollison",
            },
        }
    ]
    assert Person.get(id=person.pk).email == "bill@somewhere.org"  # Unchanged.


def test_should_not_duplicate_role_email_on_inverted_name(
    workbook, person, organisation
):
    role = Role.objects.get(person=person, organisation=organisation)
    report, persons, _ = importer.process(
        workbook([(person.last_name, person.first_name.lower(), role.email)])
    )
    assert len(persons) == 1
    assert report == [
        {
            "data": {
                "country": "FR",
                "email": "bill@somewhere.org",
                "first_name": "Bill",
                "label": "Bill Mollison",
                "last_name": "Mollison",
                "pk": person.pk,
                "resource": "person",
                "uri": f"/person/{person.pk}",
            },
            "status": "found",
            "report": {
                "first_name": "Searched 'Mollison', found 'Bill'",
                "last_name": "Searched 'bill', found 'Mollison'",
            },
            "raw": {
                "email": role.email,
                "first_name": "Mollison",
                "last_name": "bill",
            },
        }
    ]
    assert Person.get(id=person.pk).email == "bill@somewhere.org"  # Unchanged.


def test_should_not_match_if_more_than_one_person_share_same_names(workbook, person):
    Person(first_name="Mark", last_name="Knopfler").save()
    Person(first_name="Mark", last_name="Knopfler").save()
    report, persons, _ = importer.process(
        workbook([("Mark", "Knopfler", "other@email.org")])
    )
    assert persons[0] is None
    assert report == [
        {
            "data": {},
            "status": "error",
            "report": {"error": "Multiple objects with same first name and last name"},
            "raw": {
                "email": "other@email.org",
                "first_name": "Mark",
                "last_name": "Knopfler",
            },
        }
    ]


def test_should_update_empty_extra_fields(workbook, person):
    person.country = "BE"
    person.phone = None
    person.save()
    assert len(person.versions) == 2
    report, persons, _ = importer.process(
        workbook(
            [
                (
                    person.first_name,
                    person.last_name,
                    person.email,
                    "FR",
                    "+3312345678",
                    "foo",
                )
            ],
            headers=["first_name", "last_name", "email", "country", "phone", "invalid"],
        )
    )
    person = Person.objects.get(email=person.email)
    assert person.country == "BE"
    assert person.phone == "+3312345678"
    assert len(person.versions) == 3


def test_should_reverse_country(workbook, person):
    person.country = None
    person.save()
    assert len(person.versions) == 2
    report, persons, _ = importer.process(
        workbook(
            [
                (
                    person.first_name,
                    person.last_name,
                    person.email,
                    "Belgium",
                    "+3312345678",
                    "foo",
                )
            ],
            headers=["first_name", "last_name", "email", "country", "phone", "invalid"],
        )
    )
    person = Person.objects.get(email=person.email)
    assert person.country == "BE"


def test_should_cast_extra_fields(workbook, person):
    person.phone = None
    person.save()
    assert len(person.versions) == 2
    report, persons, _ = importer.process(
        workbook(
            [
                (
                    person.first_name,
                    person.last_name,
                    person.email,
                    12345678,  # An int, that should be cast to str.
                )
            ],
            headers=["first_name", "last_name", "email", "phone"],
        )
    )
    person = Person.objects.get(email=person.email)
    assert person.phone == "12345678"


def test_should_not_save_if_nothing_has_been_changed(workbook, person):
    person.country = "BE"
    person.phone = "+3312345678"
    person.save()
    assert len(person.versions) == 2
    report, persons, _ = importer.process(
        workbook(
            [
                (
                    person.first_name,
                    person.last_name,
                    person.email,
                    "FR",
                    "+3312345678",
                    "foo",
                )
            ],
            headers=["first_name", "last_name", "email", "country", "phone", "invalid"],
        )
    )
    person = Person.objects.get(email=person.email)
    assert person.country == "BE"
    assert person.phone == "+3312345678"
    assert len(person.versions) == 2


def test_should_catch_extra_fields_errors(workbook, person):
    person.country = None
    person.phone = None
    person.save()
    report, persons, _ = importer.process(
        workbook(
            [
                (
                    person.first_name,
                    person.last_name,
                    person.email,
                    "XX",
                    "+3312345678",
                    "foo",
                )
            ],
            headers=["first_name", "last_name", "email", "country", "phone", "invalid"],
        )
    )
    assert report[0]["report"] == {"error": "Invalid value for country: 'XX'"}
    person = Person.objects.get(email=person.email)
    assert person.country is None
    assert person.phone is None


def test_should_conflict_if_email_is_owned_by_an_organisation(workbook, organisation):
    organisation.email = "Owned@by.me"  # Should be case insensitive.
    organisation.save()
    report, persons, _ = importer.process(
        workbook([("Mark", "Knopfler", organisation.email.lower())])
    )
    assert persons[0] is None
    assert report == [
        {
            "data": {},
            "status": "error",
            "report": {"email": "Email found on organisation `Bill Org`"},
            "raw": {
                "email": "owned@by.me",
                "first_name": "Mark",
                "last_name": "Knopfler",
            },
        }
    ]


def test_should_find_user_if_names_match_and_email_found_on_organisation(
    workbook, organisation, person
):
    organisation.email = "Owned@by.me"  # Should be case insensitive.
    organisation.save()
    report, persons, _ = importer.process(
        workbook([(person.first_name, person.last_name, organisation.email.lower())])
    )
    assert persons[0] == person
    assert report[0]["status"] == "found"
    assert report[0]["report"] == {
        "email": "User `Bill Mollison` has another email in DB: "
        "`bill@somewhere.org`",
    }


def test_should_return_an_error_if_no_email(workbook):
    report, persons, _ = importer.process(workbook([("Mark", "Knopfler", "")]))
    assert persons[0] is None
    assert report == [
        {
            "data": {},
            "status": "error",
            "report": {"email": "Missing email"},
            "raw": {"email": "", "first_name": "Mark", "last_name": "Knopfler"},
        }
    ]


def test_should_not_match_part_of_email(workbook, person):
    person.email = "foo@bar.org"
    person.save()
    report, persons, _ = importer.process(workbook([("Mark", "Knopfler", "bar.org")]))
    assert persons[0] is None
    assert report == [
        {
            "data": {},
            "status": "error",
            "report": {"error": "Invalid value for email: 'bar.org'"},
            "raw": {
                "email": "bar.org",
                "first_name": "Mark",
                "last_name": "Knopfler",
            },
        }
    ]


def test_should_return_an_error_if_email_is_invalid(workbook, person):
    report, persons, _ = importer.process(workbook([("Mark", "Knopfler", "email.org")]))
    assert persons[0] is None
    assert report == [
        {
            "data": {},
            "status": "error",
            "report": {"error": "Invalid value for email: 'email.org'"},
            "raw": {
                "email": "email.org",
                "first_name": "Mark",
                "last_name": "Knopfler",
            },
        }
    ]


def test_should_return_an_error_if_email_is_invalid_on_name_match(workbook, person):
    person.email = None
    person.save()
    report, persons, _ = importer.process(
        workbook([(person.first_name, person.last_name, "email.org")])
    )
    assert persons[0] is None
    assert report == [
        {
            "data": {},
            "status": "error",
            "report": {"error": "Invalid value for email: 'email.org'"},
            "raw": {
                "email": "email.org",
                "first_name": person.first_name,
                "last_name": person.last_name,
            },
        }
    ]


def test_should_trim_emails(workbook, person, keyword):
    assert Person.objects(email="bob@dylan.com").count() == 0
    report, persons, _ = importer.process(
        workbook([("Bob", "Dylan", " bob@dylan.com  ")])
    )
    assert Person.objects(email="bob@dylan.com").count() == 1


def test_should_trim_firstname_and_lastname(workbook, person, keyword):
    assert Person.objects(last_name="Dylan").count() == 0
    report, persons, _ = importer.process(
        workbook([(" Bob", "Dylan  ", "bob@dylan.com")])
    )
    assert Person.objects(last_name="Dylan").count() == 1


def test_with_formula():
    assert Person.objects(email="jc@vandam.me").count() == 0
    importer.process((Path(__file__).parent / "data/with_formula.xlsx"))
    assert Person.objects(email="jc@vandam.me").count() == 1
    assert Person.objects.get(email="jc@vandam.me").comment == "Good"  # Formula result.


def test_should_trim_firstname_and_lastname_before_matching(workbook, person, keyword):
    person.first_name = "Bob"
    person.last_name = "Dylan"
    person.save()
    report, persons, _ = importer.process(
        workbook([(" Bob", "Dylan  ", "bob@dylan.com")])
    )
    # No duplicate has been created.
    assert Person.objects(last_name="Dylan").count() == 1


def test_should_match_firstname_and_lastname_with_diacritics(workbook, person, keyword):
    person.first_name = "Bøb"
    person.last_name = "Dÿlàn"
    person.email = None
    person.save()
    report, persons, _ = importer.process(
        workbook([("BOB", "Dylan", "some@thing.else")])
    )
    # No duplicate has been created.
    assert Person.objects(email="some@thing.else").count() == 1
    assert Person.objects(last_name="Dÿlàn").count() == 1
    assert Person.objects(last_name="Dylan").count() == 0
    assert report[0]["report"] == {
        "email": "Attached email `some@thing.else`",
        "first_name": "Searched 'BOB', found 'Bøb'",
        "last_name": "Searched 'Dylan', found 'Dÿlàn'",
    }


def test_should_fold_firstname_and_lastname_before_matching(workbook, person, keyword):
    person.first_name = "Bob"
    person.last_name = "Dylan"
    person.email = None
    person.save()
    report, persons, _ = importer.process(
        workbook([("BøB", "Dÿlàn", "some@thing.else")])
    )
    # No duplicate has been created.
    assert Person.objects(email="some@thing.else").count() == 1
    assert Person.objects(last_name="Dylan").count() == 1
    assert Person.objects(last_name="Dÿlàn").count() == 0
    assert report[0]["report"] == {
        "email": "Attached email `some@thing.else`",
        "first_name": "Searched 'BøB', found 'Bob'",
        "last_name": "Searched 'Dÿlàn', found 'Dylan'",
    }


def test_should_ignore_hyphen_when_matching(workbook, person, keyword):
    # Other way around
    person.first_name = "Bob-Marcel"
    person.last_name = "Dylan"
    person.email = None
    person.save()
    report, persons, _ = importer.process(
        workbook([("Bob màrcel", "Dylan", "some@thing.else")])
    )
    # No duplicate has been created.
    assert Person.objects(email="some@thing.else").count() == 1
    assert Person.objects(last_name="Dylan").count() == 1
    assert report[0]["report"] == {
        "email": "Attached email `some@thing.else`",
        "first_name": "Searched 'Bob màrcel', found 'Bob-Marcel'",
    }


def test_should_remove_hyphen_before_matching(workbook, person, keyword):
    # Other way around
    person.first_name = "Bob Marcél"
    person.last_name = "Dylan"
    person.email = None
    person.save()
    report, persons, _ = importer.process(
        workbook([("Bob-màrcel", "Dylan", "some@thing.else")])
    )
    # No duplicate has been created.
    assert Person.objects(email="some@thing.else").count() == 1
    assert Person.objects(last_name="Dylan").count() == 1
    assert report[0]["report"] == {
        "email": "Attached email `some@thing.else`",
        "first_name": "Searched 'Bob-màrcel', found 'Bob Marcél'",
    }


def test_can_match_part_of_names(workbook, person, keyword, organisation):
    organisation.email = "email@point.com"
    organisation.save()
    person.first_name = "Lyse"
    person.last_name = "Von Maelbeesh"
    person.email = None
    person.save()
    report, persons, _ = importer.process(
        workbook([("mary lysé", "Von maëlbeesh", "email@point.com")])
    )
    # No duplicate has been created.
    assert Person.objects(email="email@point.com").count() == 0
    assert Person.objects(last_name="Von Maelbeesh").count() == 1
    assert report[0]["status"] == "found"
    assert report[0]["report"] == {
        "email": "Email found on organisation `Bill Org`",
        "first_name": "Searched 'mary lysé', found 'Lyse'",
        "last_name": "Searched 'Von maëlbeesh', found 'Von Maelbeesh'",
    }


def test_should_remove_title_before_matching(workbook, person, keyword):
    # Other way around
    person.first_name = "Bob"
    person.last_name = "Dylan"
    person.email = None
    person.save()
    report, persons, _ = importer.process(
        workbook([("Dr. Bob", "Dylan", "some@thing.else")])
    )
    # No duplicate has been created.
    assert Person.objects(email="some@thing.else").count() == 1
    assert Person.objects(last_name="Dylan").count() == 1
    assert report[0]["report"] == {
        "email": "Attached email `some@thing.else`",
    }


def test_empty_first_name_should_not_fail(workbook, person, keyword):
    report, persons, _ = importer.process(
        workbook([(None, person.last_name, person.email)])
    )
    assert report[0]["status"] == "found"


def test_attach_to_organisation_by_name(workbook, person, organisation, keyword):
    assert Person.objects(email="bob@dylan.com").count() == 0
    report, persons, _ = importer.process(
        workbook(
            [("Bob", "Dylan", "bob@dylan.com", organisation.name, "King")],
            headers=["first_name", "last_name", "email", "organisation", "position"],
        )
    )
    assert Person.objects(email="bob@dylan.com").count() == 1
    assert Role.get(organisation=organisation, person=persons[0])
    assert Role.get(organisation=organisation, person=persons[0]).name == "King"
    assert report[0]["report"] == {
        "organisation": "Attached to organisation 'Bill Org'"
    }


def test_organisation_name_does_not_exists(workbook, organisation, keyword):
    assert Person.objects(email="bob@dylan.com").count() == 0
    report, persons, _ = importer.process(
        workbook(
            [("Bob", "Dylan", "bob@dylan.com", "wrong name", "King")],
            headers=["first_name", "last_name", "email", "organisation", "position"],
        )
    )
    created = Person.objects.get(email="bob@dylan.com")
    assert not created.roles
    assert report[0]["report"] == {
        "organisation": "No organisation matching 'wrong name'"
    }


def test_attach_to_organisation_by_acronym(workbook, person, organisation, keyword):
    organisation.acronym = "ABCD"
    organisation.save()
    report, persons, _ = importer.process(
        workbook(
            [("Bob", "Dylan", "bob@dylan.com", "ABCD")],
            headers=["first_name", "last_name", "email", "organisation"],
        )
    )
    assert Person.objects(email="bob@dylan.com").count() == 1
    assert Role.get(organisation=organisation, person=persons[0])
    assert report[0]["report"] == {
        "organisation": "Attached to organisation 'Bill Org'"
    }


def test_attach_to_organisation_by_email(workbook, person, organisation, keyword):
    organisation.email = "contact@mydomain.org"
    organisation.save()
    report, persons, roles = importer.process(
        workbook([("Bob", "Dylan", "dylan@mydomain.org")])
    )
    person = Person.get(persons[0].pk)
    assert roles[0]
    # Email has been attached to the role, as it matches the organisation domain.
    assert person.email is None
    role = Role.get(organisation=organisation, person=persons[0])
    assert role.email == "dylan@mydomain.org"
    assert report[0]["report"] == {
        "organisation": "Attached to organisation 'Bill Org'"
    }


def test_should_not_attach_to_organisation_with_generic_email(
    workbook, person, organisation, keyword
):
    organisation.email = "contact@gmail.com"
    organisation.save()
    report, persons, roles = importer.process(
        workbook([("Bob", "Dylan", "dylan@gmail.com")])
    )
    assert roles[0] is None
    assert not Role.objects(organisation=organisation, person=persons[0])


def test_attach_to_organisation_by_website(workbook, person, organisation, keyword):
    organisation.website = "https://mydomain.org/index.html"
    organisation.save()
    assert Person.objects(email="bob@dylan.com").count() == 0
    report, persons, _ = importer.process(
        workbook([("Bob", "Dylan", "dylan@mydomain.org")])
    )
    person = Person.get(persons[0].pk)
    role = Role.get(organisation=organisation, person=persons[0])
    assert role.email == "dylan@mydomain.org"
    assert report[0]["report"] == {
        "organisation": "Attached to organisation 'Bill Org'"
    }
    # Email has been attached to the role, as it matches the organisation domain.
    assert person.email is None


def test_multiple_organisation_match_domain(workbook, organisation, keyword):
    organisation.website = "https://mydomain.org/index.html"
    organisation.save()
    Organisation(name="Foo", website="https://sub.mydomain.org").save()
    assert Person.objects(email="bob@dylan.com").count() == 0
    report, persons, _ = importer.process(
        workbook([("Bob", "Dylan", "dylan@mydomain.org")])
    )
    person = Person.get(persons[0].pk)
    assert not Role.objects(person=persons[0])
    assert person.email == "dylan@mydomain.org"
    assert report[0]["report"] == {
        "organisation": "Multiple organisation matching 'mydomain.org': 2,3"
    }

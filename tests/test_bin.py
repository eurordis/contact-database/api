from datetime import timedelta
from pathlib import Path

import pytest
from eurordis import config
from eurordis.bin import hard_delete, import_extra_diseases, chore_report
from eurordis.helpers import utcnow
from eurordis.models import Contact, Disease, Organisation, Person, Role
import io
import sys


@pytest.fixture(autouse=True, scope="function")
def module_setup_teardown(keyword):
    config.TOBEDELETED_KEYWORD = keyword.pk


def test_hard_delete(bot, person, organisation, person2, keyword):
    person2.keywords = [keyword.pk]
    person2.save()
    organisation.keywords = [keyword.pk]
    organisation.save()
    Contact.objects(pk__in=[person2.pk, organisation.pk]).update(
        modified_at=utcnow() - timedelta(days=8)
    )
    hard_delete()
    contacts = Contact.objects.all()
    assert len(contacts) == 2
    assert {bot.pk, person.pk} == set([c.pk for c in contacts])


def test_hard_delete_cant_deleted_linked_person(bot, person, organisation, keyword):
    assert Contact.objects.count() == 3
    person.keywords = [keyword.pk]
    person.save()
    Contact.objects(pk=person.pk).update(modified_at=utcnow() - timedelta(days=8))
    hard_delete()
    assert Contact.objects.count() == 3


def test_hard_delete_do_not_deleted_recent_soft_delete(
    bot, person, organisation, keyword
):
    assert Contact.objects.count() == 3
    organisation.keywords = [keyword.pk]
    organisation.save()
    Contact.objects(pk=organisation.pk).update(modified_at=utcnow() - timedelta(days=6))
    hard_delete()
    assert Contact.objects.count() == 3


def test_import_extra_diseases(disease, organisation):
    config.ROOT = Path(__file__).parent
    config.NON_RARE_ORPHA_NUMBER = disease.pk
    d1 = Disease(roots=[disease], ancestors=[disease], orpha_number=1, name="D1").save()
    Disease(roots=[disease], ancestors=[disease], orpha_number=2, name="D2").save()
    d3 = Disease(roots=[disease], ancestors=[disease], orpha_number=3, name="D3").save()
    d4 = Disease(roots=[disease], ancestors=[disease], orpha_number=4, name="D4").save()
    organisation.diseases = [d4]
    organisation.save()
    assert Disease.objects.count() == 5
    import_extra_diseases()
    assert Disease.objects.count() == 5
    d1.reload()
    d3.reload()
    disease.reload()
    assert d1.name == "new name"
    assert d1.status == "non rare"
    assert d3.name == "D3"
    with pytest.raises(Disease.DoesNotExist):
        Disease.get(2)
    assert Disease.get(4)  # Not delete, because linked to some contact.
    created = Disease.get(5)
    assert created.name == "D5"
    assert created.status == "obsolete"
    assert created.roots == [disease]
    assert created.ancestors == [disease]
    assert created in disease.descendants

def test_chore_report_multiple_organisations_no_default():
    # Prerequisites
    person = Person(
        first_name="Bill",
        last_name="Mollison",
        email="bill@somewhere.org",
        phone="+9876543210",
        cell_phone="+611111111",
        country="FR",
        languages=["en", "fr"]
    ).save()
    assert person.first_name == "Bill"
    assert person.email == "bill@somewhere.org"

    org1 = Organisation(
        name="Organisation One",
        email="org1@contact.org",
        country="FR",
        kind=60
    ).save()
    org2 = Organisation(
        name="Organisation Two",
        email="org2@contact.org",
        country="FR",
        kind=60
    ).save()
    assert org1.name == "Organisation One"
    assert org2.name == "Organisation Two"

    # Roles with no default & 1 that is archived
    role1 = Role(
        person=str(person.id),
        organisation=str(org1.id),
        is_primary=True,
        is_default=False,
        is_voting=True,
        is_archived=False,
        events=[]
    ).save()
    role2 = Role(
        person=str(person.id),
        organisation=str(org2.id),
        is_primary=False,
        is_default=False,
        is_voting=False,
        is_archived=True,  # This organisation is archived & doesn't show on frontend so shouldn't show in the report
        events=[]
    ).save()
    assert role1.organisation.id == str(org1.id)
    assert role2.organisation.id == str(org2.id)

    # Create the EURORDIS organization (so that chore_reports can run properly)
    eurordis = Organisation(
        id="1554",
        name="EURORDIS",
        email="Verla.Beatty38@hotmail.com",
        country="FR",
        kind=60,
        created_by="1",
        modified_by="3",
        version=1611,
        address="2758 A Street",
        postcode="65451",
        city="South Emmalee",
        website="https://example.com/",
        membership=[
            {"organisation": org1.id, "status": "f", "fees": []},
            {"organisation": org2.id, "fees": []}
        ],
        creation_year=1997,
        board="",
    ).save()
    assert eurordis.name == "EURORDIS"

    # Validate chore_report output
    captured_output = io.StringIO()
    sys.stdout = captured_output
    try:
        chore_report()
    finally:
        sys.stdout = sys.__stdout__
    output = captured_output.getvalue()
    expected_output = """Sending email Content-Type: text/plain; charset="utf-8"
Content-Transfer-Encoding: 7bit
MIME-Version: 1.0
Subject: Contact Database Report
From: contact-database@eurordis.org
Reply-To: contact-database@eurordis.org
To: contact-database@eurordis.org

Hi Eurordis Staff,

Here is your database chore report.

Full members without a primary contact:

None

Full members without a voting contact:

None

Duplicate emails in roles:

None

Emails from roles that also are in contacts:

None

Diseases linked to contacts but without grouping:

None

Persons with multiple organisations but no default one:

None

Persons with a single organisation which is set as default :

None

Your Devoted Little Bot
"""
    assert output.strip() == expected_output.strip()

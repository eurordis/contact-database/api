import pytest
from eurordis.models import Contact, Disease, Organisation, Person


def test_persons_from_disease(person, disease):
    person.diseases.append(disease)
    person.save()
    assert person in Person.objects.from_disease(str(disease.pk))


def test_contacts_from_disease_with_class_filter(person, disease, organisation):
    person.diseases.append(disease)
    person.save()
    organisation.diseases.append(disease)
    organisation.save()
    assert person in Person.objects.from_disease(str(disease.pk))
    assert organisation in Organisation.objects.from_disease(str(disease.pk))
    assert person in Contact.objects.from_disease(str(disease.pk))
    assert organisation in Contact.objects.from_disease(str(disease.pk))
    assert organisation not in Person.objects.from_disease(str(disease.pk))
    assert person not in Organisation.objects.from_disease(str(disease.pk))
    assert organisation not in Person.objects.from_disease(str(disease.pk))
    assert person not in Organisation.objects.from_disease(str(disease.pk))


def test_contacts_from_disease_and_family(person, organisation, disease):
    parent = Disease(
        name="Alexander disease", orpha_number="12346", descendants=[disease]
    ).save()
    disease.ancestors = [parent]
    disease.save()
    person.diseases.append(disease)
    person.save()
    organisation.diseases.append(parent)
    organisation.save()
    assert person in Contact.objects.from_disease(str(disease.pk))
    assert organisation not in Contact.objects.from_disease(str(disease.pk))
    assert organisation in Contact.objects.from_disease(f"<{disease.pk}")
    assert person not in Contact.objects.from_disease(str(parent.pk))
    assert person in Contact.objects.from_disease(f">{parent.pk}")


def test_contacts_from_disease_with_invalid_pattern(person, disease):
    with pytest.raises(ValueError):
        Contact.objects.from_disease(">>!12345")


def test_disease_ft(disease):
    disease.name = "Cystic Fibrosis"
    disease.synonyms = ["Mucoviscidosis"]
    disease.save()
    assert disease.ft == [
        {
            "score": 1,
            "trigrams": [
                "^ci",
                "cis",
                "ist",
                "sti",
                "tik",
                "^fi",
                "fib",
                "ibr",
                "bro",
                "roz",
                "ozi",
                "zis",
            ],
        },
        {
            "score": 0.9,
            "trigrams": [
                "^mu",
                "muk",
                "uko",
                "kov",
                "ovi",
                "vis",
                "isc",
                "sci",
                "cid",
                "ido",
                "doz",
                "ozi",
                "zis",
            ],
        },
    ]


def test_search(disease):
    disease.name = "primary bone dysplasia"
    disease.save()
    assert disease in Disease.objects.search("primary")
    assert disease in Disease.objects.search("prima dysp")


def test_disease_grouping(disease):
    disease.groupings = [{"name": "ernica"}]
    disease.save()
    assert Disease.objects.get(pk=disease.pk).groupings[0].name == "ernica"


def test_updating_grouping_should_update_related_contacts(disease, person, organisation):
    person.diseases = [disease]
    person.save()
    assert len(person.history) == 3
    organisation.diseases = [disease]
    organisation.save()
    assert len(organisation.history) == 3
    disease.groupings = [{"name": "ernica"}]
    disease.save()
    person.reload()
    organisation.reload()
    assert "ernica" in person.groupings
    assert "ernica" in organisation.groupings
    # groupings is not an editable field, so history should not have been changed
    assert len(person.history) == 3
    assert len(organisation.history) == 3


def test_cannot_delete_disease_linked_to_contacts(person, disease):
    person.diseases = [disease]
    person.save()
    with pytest.raises(ValueError):
        disease.delete()
    person.diseases = []
    person.save()
    disease.delete()

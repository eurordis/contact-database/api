import pytest
from eurordis import helpers


@pytest.mark.parametrize(
    "input,expected",
    [("cistic", ["^ci", "cis", "ist", "sti", "tic"]), ("1", ["^1"]), ("12", ["^12"])],
)
def test_ngrams(input, expected):
    assert helpers.ngrams(input) == expected


@pytest.mark.parametrize(
    "input,expected",
    [
        ("THis is «crazy»!", "tis is krazi"),
        ("A.R.M.G.H", "armg"),
        ("Gala_Black", "gala blak"),
        ("Alpha-1", "alfa 1"),
    ],
)
def test_fold(input, expected):
    assert helpers.fold(input) == expected


@pytest.mark.parametrize(
    "input,expected",
    [
        ("cystic", "cistik"),
        ("fibrosis", "fibrozis"),
        ("mannosidase", "manozidaze"),
        ("tumour", "tumor"),
        ("haemato", "emato"),
        ("volunteers", "volunter"),
        ("deafness", "deafnes"),
        ("bonnemann meinecke", "boneman meineke"),
        ("arthrogryposis", "artrogripozis"),
        ("hemiatrophy", "emiatrofi"),
        ("mucopolysaccharidosis", "mukopolizakaridozis"),
        ("von willebrand", "von vilebrand"),
        ("squamous", "skamus"),
        ("brocq", "brok"),
        ("hypertension", "ipertension"),
    ],
)
def test_phonemicize(input, expected):
    assert helpers.phonemicize(input) == expected


@pytest.mark.parametrize(
    "input,expected",
    (
        ("Bone", "bond"),
        ("cance", "euracan"),
        ("card", "guardheart"),
        ("musc", "euronmd"),
    ),
)
def test_complete_grouping(input, expected):
    assert helpers.complete_grouping(input)[0]["pk"] == expected


@pytest.mark.parametrize("input,expected", (("belg", "BE"), ("uni kin", "GB")))
def test_complete_country(input, expected):
    assert helpers.complete_country(input)[0]["pk"] == expected


@pytest.mark.parametrize(
    "q,label,score",
    (
        ("belg", "Belgium", 0.9),
        ("belgium", "Belgium", 1),
        ("belgium", "HAE Belgium", 0.78),
        ("Lau gro", "Laurent Grossetête", 0.74),
        ("black pearl", "Gala Black Pearl Awards 2018", 0.74),
        ("rare 2030", "Rare2030 panel of experts", 0.9),
    ),
)
def test_compare_str(q, label, score):
    assert helpers.compare_str(q, label) == score


@pytest.mark.parametrize(
    "wanted,available,expected",
    [
        (["fr", "it"], ["en", "es", "fr", "it", "de"], "fr"),
        (["fr", "it"], ["en", "es", "it", "de"], "it"),
        (["ru", "ar"], ["en", "es", "it", "de"], "en"),
        ([], ["en", "es", "it", "de"], "en"),
    ],
)
def test_choose_language(wanted, available, expected):
    assert helpers.choose_language(wanted, available) == expected


@pytest.mark.parametrize(
    "input,expected",
    [
        ("VU", "fr"),
        ("", "en"),
        (None, "en"),
    ],
)
def test_lang_from_country(input, expected):
    assert helpers.lang_from_country(input) == expected


@pytest.mark.parametrize(
    "input,expected",
    [
        ("DR Edith", "Edith"),
        ("Dr. Bob", "Bob"),
        ("Dravic", "Dravic"),
        ("M. Adam", "Adam"),
        ("Me Corbeau", "Corbeau"),
        ("Meme", "Meme"),
        ("M Renard", "Renard"),
        ("Pr Tournesol", "Tournesol"),
    ],
)
def test_clean_title(input, expected):
    assert helpers.clean_title(input) == expected

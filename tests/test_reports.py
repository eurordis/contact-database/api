from datetime import date
from io import BytesIO

from eurordis import config
from eurordis.models import Event, Group, Organisation, Role, RoleToEvent
from eurordis.reports import (all_attendees, dump_roles, epag_members,
                              representatives_stats, voting)
from openpyxl import load_workbook

def test_export_event_attendees(person, person2, person3, event, keyword, role, organisation):
    event2 = Event(
        name="Event2",
        location="Toussacq",
        start_date=date(2019, 12, 9),
        kind="ecrd",
        duration=2,
    ).save()
    role.events.append(RoleToEvent(event=event, role="Curious"))
    role.events.append(RoleToEvent(event=event2, role="Tourist"))
    role.save()
    Role(
        person=person2,
        organisation=organisation,
        events=[RoleToEvent(event=event), RoleToEvent(event=event2, role="Showman")],
    ).save()
    Role(
        person=person3,
        organisation=organisation,
        events=[RoleToEvent(event=event), RoleToEvent(event=event2, role="Showman")],
    ).save()
    wb = load_workbook(filename=BytesIO(all_attendees()))
    assert list(wb.active.values) == [
        (
            "pk",
            "first_name",
            "last_name",
            "email",
            "role",
            "kind of event",
            "event name",
            "event start date",
            "event duration",
            "event location",
            "organisation",
        ),
        (
            person.pk,
            "Bill",
            "Mollison",
            "bmollison@bill.org",
            "Curious",
            "Awards ceremony",
            "Royal Gala",
            event.start_date.isoformat(),
            8,
            "Bruxelles",
            "Bill Org",
        ),
        (
            person.pk,
            "Bill",
            "Mollison",
            "bmollison@bill.org",
            "Tourist",
            "ECRD - European Conference on Rare Diseases",
            "Event2",
            event2.start_date.isoformat(),
            2,
            "Toussacq",
            "Bill Org",
        ),
        (
            person2.pk,
            "David",
            "Holmgren",
            "david@somewhere.org",
            None,
            "Awards ceremony",
            "Royal Gala",
            event.start_date.isoformat(),
            8,
            "Bruxelles",
            "Bill Org",
        ),
        (
            person2.pk,
            "David",
            "Holmgren",
            "david@somewhere.org",
            "Showman",
            "ECRD - European Conference on Rare Diseases",
            "Event2",
            event2.start_date.isoformat(),
            2,
            "Toussacq",
            "Bill Org",
        ),
        (
            person3.pk,
            "Patrick",
            "Morel",
            "contact@bill.org",
            None,
            "Awards ceremony",
            "Royal Gala",
            event.start_date.isoformat(),
            8,
            "Bruxelles",
            "Bill Org",
        ),
        (
            person3.pk,
            "Patrick",
            "Morel",
            "contact@bill.org",
            "Showman",
            "ECRD - European Conference on Rare Diseases",
            "Event2",
            event2.start_date.isoformat(),
            2,
            "Toussacq",
            "Bill Org",
        ),
    ]


def test_voting_export(person, person2, organisation):
    other = Organisation(name="other").save()
    Role.get(person=person, organisation=organisation).delete()
    person.roles = [
        Role(person=person, organisation=organisation, is_voting=True),
    ]
    person.save()
    person2.roles = [
        Role(person=person2, organisation=other, is_voting=True, email="cor@pora.te"),
    ]
    person2.save()
    wb = load_workbook(filename=BytesIO(voting()))
    assert list(wb.active.values) == [
        ("pk", "name", "country", "contact", "email"),
        ("3", "Bill Org", "Belgium", "Bill Mollison", "bill@somewhere.org"),
        ("4", "other", None, "David Holmgren", "cor@pora.te"),
    ]


def test_epag_members(person, organisation, group):
    config.EPAG_GROUP = group.pk
    group2 = Group(name="ERN - whatever", kind="epag").save()
    person.roles[0].add_group(group=group).save()
    person.roles[0].add_group(group=group2, role="Patient Representative").save()
    report = epag_members()
    assert report == [
        {
            "country": "FR",
            "ern": "ERN - whatever",
            "fullname": "Bill Mollison",
            "organisation": "Bill Org",
            "website": None,
        }
    ]


def test_representatives_stats(person, organisation, group):
    config.EPAG_GROUP = group.pk
    group2 = Group(name="ERN - whatever", kind="epag").save()
    person.roles[0].add_group(group=group).save()
    person.roles[0].add_group(group=group2, role="Patient Representative").save()
    wb = load_workbook(filename=BytesIO(representatives_stats()))
    print(list(wb.active.values))
    assert list(wb.active.values) == [
        ("country", "Count of person_pk", None, "group_name", "Count of person_pk"),
        ("France", 1, None, "ERN - whatever", 1),
        ("Total", 1, None, "Total", 1)
    ]

def test_dump_roles(person, role, person2):
    organisation2 = Organisation(
        name="David Org",
        email="contact@david.org",
        country="FR",
        kind=60,
    )
    organisation2.roles = [
        Role(
            name="CEO",
            is_default=True,
            person=person2.pk,
            organisation=organisation2,
        ),
        Role(
            name="CEO",
            is_archived=True,
            person=person.pk,
            organisation=organisation2,
        )

    ]
    organisation2.save()
    wb = load_workbook(filename=BytesIO(dump_roles()))
    assert list(wb.active.values) == [
        ("person_pk", "first_name", "last_name", "email", "role", "phone", "is_primary", "is_default", "is_voting", "organisation_pk", "organisation", "kind", "groups",),
        ("1", "Bill", "Mollison", "bmollison@bill.org", "CEO", "+1234567890", "1", None, None, "2", "Bill Org", "Patient Organisation", None),
        ("3", "David", "Holmgren", "contact@david.org", "CEO", None, None, "1", None, "4", "David Org", "Patient Organisation", None),
    ]

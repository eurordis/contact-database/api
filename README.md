# Eurordis Base Contact

## System dependencies

- python >= 3.6
- MongoDB >= 4.x


## Install

- create a venv: `python3 -m venv path/to/your/new/venv`
- source your venv: `source path/to/your/new/venv/bin/activate`
- run `make develop`
- install indexes: `eurordis create-indexes`


## Run local server

- start MongoDB:
    - OSX using Homebrew: `brew services start mongodb-community`
    - Windows: `mongod.exe`
    - Unix : `sudo systemctl start mongod`
- start roll server: `eurordis serve --reload`


## Run tests

`make test`


## Import Orphadata

    eurordis download-orphadata
    eurordis import-orphadata


## Import FileMaker data

- in FileMaker's menu, select `Scripts` > `Scopyleft Export`
- move exported files from `This PC/Downloads/` (in the remote computer) to `contact-database/data/legacy/` (in the project)
